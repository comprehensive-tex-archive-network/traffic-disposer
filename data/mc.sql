 id |           name           |     name_english      | continent_id 
----+--------------------------+-----------------------+--------------
 43 | Argentina                | Argentina             |            7
  0 | dummy                    | dummy                 |            0
  2 | Belgie                   | Belgium               |            5
  6 | Denmark                  | Denmark               |            5
  7 | Deutschland              | Germany               |            5
  8 | England                  | England               |            5
  9 | Espa~na                  | Spain                 |            5
 10 | Estonia                  | Estonia               |            5
 11 | Suomi                    | Finland               |            5
 12 | France                   | France                |            5
 18 | Eire                     | Ireland               |            5
 19 | Italia                   | Italy                 |            5
 22 | Norge                    | Norway                |            5
 24 | Portugal                 | Portugal              |            5
 25 | Romania                  | Romania               |            5
 28 | Slovak Republic          | Slovak Republic       |            5
 31 | Schweiz                  | Switzerland           |            5
 33 | Czech Republic           | The Czech Republic    |            5
 38 | Österreich               | Austria               |            5
  1 | Australia                | Australia             |            4
 39 | Chile                    | Chile                 |            7
 42 | Brasil                   | Brazil                |            7
  3 | Canada                   | Canada                |            6
 37 | USA                      | USA                   |            6
  4 | China                    | China                 |            3
 14 | Hong Kong                | Hong Kong             |            3
 16 | India                    | India                 |            3
 17 | Indonesia                | Indonesia             |            3
 20 | Nippon                   | Japan                 |            3
 27 | Singapore                | Singapore             |            3
 32 | Taiwan                   | Taiwan                |            3
 40 | New Zealand              | New Zealand           |            4
  5 | Costa Rica               | Costa Rica            |            6
 29 | South Africa             | South Africa          |            1
 46 | Columbia                 | Colombia              |            7
 47 | Serbia and Montenegro    | Serbia and Montenegro |            5
 41 | México                   | Mexico                |            6
 48 | Malaysia                 | Malaysia              |            3
 49 | Lietuva                  | Lithuania             |            5
 50 | Slovenija                | Slovenia              |            5
 52 | מדינת ישראל              | Israel                |            3
 53 | Byelarus'                | Belarus               |            5
 54 | Srbija                   | Serbia                |            5
 55 | Pilipinas                | Philippines           |            3
 56 | al-Jazā’ir               | Algeria               |            1
 57 | المملكة العربية السعودية | Saudi Arabia          |            3
 58 | Nouvelle-Calédonie       | New Caledonia         |            4
 59 | Việt Nam                 | Vietnam               |            3
 45 | ايران                    | Iran                  |            3
 44 | پاکستان                  | Pakistan              |            3
 15 | Magyarország             | Hungary               |            5
 13 | Ελλάς                    | Greece                |            5
 23 | Polska                   | Poland                |            5
 26 | Россия                   | Russia                |            5
 30 | Sverige                  | Sweden                |            5
 51 | ประเทศไทย                | Thailand              |            3
 35 | Türkiye                  | Turkey                |            5
 34 | Nederland                | The Netherlands       |            5
 60 | المغرب                   | Morocco               |            1
 36 | United Kingdom           | United Kingdom        |            5
 21 | Han'guk                  | Korea                 |            3
(61 rows)

