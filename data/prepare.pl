#!/bin/perl

my %country = (
    43 => 'AR', #  Argentina             |            7
     2 => 'BE', #  Belgium               |            5
     6 => 'DK', #  Denmark               |            5
     7 => 'DE', #  Germany               |            5
     8 => 'GB', #  England               |            5
     9 => 'ES', #  Spain                 |            5
    10 => 'EE', #  Estonia               |            5
    11 => 'FI', #  Finland               |            5
    12 => 'FR', #  France                |            5
    18 => 'IE', #  Ireland               |            5
    19 => 'IT', #  Italy                 |            5
    22 => 'NO', #  Norway                |            5
    24 => 'PT', #  Portugal              |            5
    25 => 'RO', #  Romania               |            5
    28 => 'SK', #  Slovak Republic       |            5
    31 => 'CH', #  Switzerland           |            5
    33 => 'CZ', #  The Czech Republic    |            5
    38 => 'AT', #  Austria               |            5
     1 => 'AU', #  Australia             |            4
    39 => 'CL', #  Chile                 |            7
    42 => 'BR', #  Brazil                |            7
     3 => 'CA', #  Canada                |            6
    37 => 'US', #  USA                   |            6
     4 => 'CN', #  China                 |            3
    14 => 'HK', #  Hong Kong             |            3
    16 => 'IN', #  India                 |            3
    17 => 'ID', #  Indonesia             |            3
    20 => 'JP', #  Japan                 |            3
    27 => 'SG', #  Singapore             |            3
    32 => 'TW', #  Taiwan                |            3
    40 => 'NZ', #  New Zealand           |            4
     5 => 'CR', #  Costa Rica            |            6
    29 => 'ZA', #  South Africa          |            1
    46 => 'CO', #  Colombia              |            7
    47 => '', #  Serbia and Montenegro |            5
    41 => 'MX', #  Mexico                |            6
    48 => 'MY', #  Malaysia              |            3
    49 => 'LT', #  Lithuania             |            5
    50 => 'SI', #  Slovenia              |            5
    52 => 'IL', #  Israel                |            3
    53 => 'BY', #  Belarus               |            5
    54 => 'RS', #  Serbia                |            5
    55 => 'PH', #  Philippines           |            3
    56 => 'DZ', #  Algeria               |            1
    57 => 'SA', #  Saudi Arabia          |            3
    58 => 'NC', #  New Caledonia         |            4
    59 => 'VN', #  Vietnam               |            3
    45 => 'IR', #  Iran                  |            3
    44 => 'PK', #  Pakistan              |            3
    15 => 'HU', #  Hungary               |            5
    13 => 'GR', #  Greece                |            5
    23 => 'PL', #  Poland                |            5
    26 => 'RU', #  Russia                |            5
    30 => 'SE', #  Sweden                |            5
    51 => 'TH', #  Thailand              |            3
    35 => 'TR', #  Turkey                |            5
    34 => 'NL', #  The Netherlands       |            5
    60 => 'MA', #  Morocco               |            1
    36 => 'GB', #  United Kingdom        |            5
    21 => 'KR', #  Korea                 |            3
    );

my %status = (
    0 => '???', # | no status
    1 => 'MASTER', # | master
    2 => 'ACTIVE', # | active
    3 => 'PARTIAL', # | partial
    4 => 'INACTIVE', # | inactive
    5 => 'WITHDRAWN', # | withdrawn
    6 => '666' # | no reaction from maintainer
    );
my %mirror = ();
my $pre = '';

# INSERT INTO mirror (id, name, status, country, region, town, notes) 
# VALUES
#         (1, 'ctan.org', 'MASTER', 'DE', 'NRW', 'Köln', 'Root of all evil');

print "INSERT INTO mirror (id, name, status, country, region, town, notes)\nVALUES";

while (<>) {
    chomp;
    my @attr = split(/ *\| */);
    if ( $attr[0] > 0 ) {
	print $pre, "(",
	    $attr[0], ", '$attr[1]', '",
	    $status{$attr[9]}, "', '",
	    $country{$attr[2]}, "', '",
	    $attr[3], "', '",
	    $attr[4], "', '",
	    $attr[10], "')";
	$pre = ",\n\t";
    }
}

print ";\n";

#
