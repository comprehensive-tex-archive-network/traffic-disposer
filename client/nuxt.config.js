import colors from 'vuetify/es5/util/colors'

export default {
  /*
  ** Nuxt rendering mode
  ** See https://nuxtjs.org/api/configuration-mode
  */
  mode: 'spa',

  /*
  ** Nuxt target
  ** See https://nuxtjs.org/api/configuration-target
  */
  target: 'server',

  /*
  ** Headers of the page
  ** See https://nuxtjs.org/api/configuration-head
  */
  head: {
    titleTemplate: '%s',
    title: '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/traffic-disposer-logo.png' }
    ]
  },

  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    '@/static/fonts/opensans/opensans.css'
  ],
  /*
  ** Plugins to load before mounting the App
  ** https://nuxtjs.org/guide/plugins
  */
  plugins: [
  ],
  /*
  ** Auto import components
  ** See https://nuxtjs.org/api/configuration-components
  */
  components: true,
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/vuetify'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/pwa' ,
    ['nuxt-i18n', {
      defaultLocale: 'en',
      locales: [
        {
          code: 'en',
          name: 'English',
          file: 'en.js'
        },
        {
          code: 'de',
          name: 'Deutsch',
          file: 'de.js'
        }
      ],
      lazy: true,
      strategy: 'no_prefix',
      langDir: 'i18n/',
      detectBrowserLanguage: {
        cookieKey: 'i18n'
      }
    }]
  ],

  /*
  ** vuetify module configuration
  */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    defaultAssets: {
      font: {
        family: 'opensans' 
      } // ,
      // icons: 'fa'
    },
    theme: {
      dark: false,
      themes: {
        light: {
          primary: '#339',
          secondary: '#99f',
          accent: '#933',
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        },
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },

  /*
  ** Axios module configuration
  */
  axios: {
    proxy: true
  },

  proxy: {
    '/api/': {
      target: 'http://localhost:8080',
      secure: false
    }
  },

  /*
  ** Build configuration
  */
  build: {
  }
}
