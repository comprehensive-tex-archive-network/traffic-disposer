/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */
export default {

  '/continent': {
    AF: 'Afrika',
    AS: 'Asien',
    EU: 'Europa',
    NA: 'Nordamerika',
    OC: 'Ozeanien',
    SA: 'Südamerika'
  },

  '/language': {
    de: 'Deutsch',
    en: 'English',
    es: 'Español',
    fr: 'Française',
    it: 'Italiano'
  },

  '/segment': {
    '/': 'Master',
    AF: 'Afrika',
    AS: 'Asien',
    EU: 'Europa',
    NA: 'Nordamerika',
    OC: 'Ozeanien',
    SA: 'Südamerika'
  },

  '/status': {
    '': '',
    MASTER: 'Master',
    ACTIVE: 'Aktiv',
    PARTIAL: 'Partiell',
    INACTIVE: 'Inaktive',
    WITHDRAWN: 'Zurückgezogen'
  },

  '/country': {
    AD: 'Andorra',
    AE: 'Vereiniget Arabische Emirate',
    AF: 'Afghanistan',
    AG: 'Antigua und Barbuda',
    AI: 'Anguilla',
    AL: 'Albanien',
    AM: 'Armenien',
    AN: 'Niederländische Antillen',
    AO: 'Angola',
    AQ: 'Antarctika (die Gebiete südlich des 60″S)',
    AR: 'Argentinien',
    AS: 'Amerikanisch Samoa',
    AT: 'Österreich',
    AU: 'Australien',
    AW: 'Aruba',
    AX: 'Åland Islands',
    AZ: 'Azerbaijan',
    BA: 'Bosnia and Herzegovina',
    BB: 'Barbados',
    BD: 'Bangladesh',
    BE: 'Belgien',
    BF: 'Burkina Faso',
    BG: 'Bulgarien',
    BH: 'Bahrain',
    BI: 'Burundi',
    BJ: 'Benin',
    BL: 'Saint Barthelemy',
    BM: 'Bermuda',
    BN: 'Brunei Darussalam',
    BO: 'Bolivien',
    BQ: 'Bonaire, Sint Eustatius und Saba',
    BR: 'Brasilien',
    BS: 'Bahamas',
    BT: 'Bhutan',
    BV: 'Bouvet Island (Bouvetoya)',
    BW: 'Botswana',
    BY: 'Weißrussland',
    BZ: 'Belize',
    CA: 'Kanada',
    CC: 'Cocos (Keeling) Inseln',
    CD: 'Kongo, Demokratische Republik',
    CF: 'Zentralafrikanische Republik',
    CG: 'Kongo, Republik',
    CH: 'Schweiz',
    CI: 'Elfenbeinküste',
    CK: 'Cook Islands',
    CL: 'Chile',
    CM: 'Kameroon',
    CN: 'China, Volksrepublik',
    CO: 'Kolombien',
    CR: 'Costa Rica',
    CU: 'Kuba',
    CV: 'Cape Verde',
    CW: 'Curaçao',
    CX: 'Weihnachtsinseln',
    CY: 'Zypern',
    CZ: 'Tschechei',
    DE: 'Deutschland',
    DJ: 'Dschibuti',
    DK: 'Dänemark',
    DM: 'Dominica',
    DO: 'Dominikanische Republik',
    DZ: 'Algerien',
    EC: 'Ecuador',
    EE: 'Estland',
    EG: 'Ägypten',
    EH: 'West-Sahara',
    ER: 'Eritrea',
    ES: 'Spanien',
    ET: 'Äthiopien',
    FI: 'Finnland',
    FJ: 'Fiji',
    FK: 'Falkland (Malvinas)',
    FM: 'Mikronesien',
    FO: 'Färöer',
    FR: 'Frankreich',
    GA: 'Gabun',
    GB: 'Großbritannien',
    GD: 'Grenada',
    GE: 'Georgien',
    GF: 'Französisch Guaina',
    GG: 'Guernsey',
    GH: 'Ghana',
    GI: 'Gibraltar',
    GL: 'Grönland',
    GM: 'Gambia',
    GN: 'Guinea',
    GP: 'Guadeloupe',
    GQ: 'Äquatorial Guinea',
    GR: 'Griechenland',
    // TODO
    GS: 'South Georgia and the South Sandwich Islands',
    GT: 'Guatemala',
    GU: 'Guam',
    GW: 'Guinea-Bissau',
    GY: 'Guyana',
    HK: 'Hong Kong',
    HM: 'Heard Island and McDonald Islands',
    HN: 'Honduras',
    HR: 'Croatia',
    HU: 'Ungarn',
    HT: 'Haiti',
    ID: 'Indonesien',
    IE: 'Irland',
    IL: 'Israel',
    IM: 'Isle of Man',
    IN: 'Indien',
    IO: 'British Indian Ocean Territory (Chagos Archipelago)',
    IQ: 'Irak',
    IR: 'Iran',
    IS: 'Island',
    IT: 'Italien',
    JE: 'Jersey',
    JM: 'Jamaika',
    JO: 'Jordanien',
    JP: 'Japan',
    KE: 'Kenia',
    KG: 'Kyrgyz Republic', // TODO
    KH: 'Cambodia', // TODO
    KI: 'Kiribati',
    KM: 'Komoroen',
    KN: 'Saint Kitts and Nevis',
    KP: 'Korea, Demokratische Volksrepublik',
    KR: 'Korea, Republik',
    KW: 'Kuwait',
    KY: 'Cayman Islands', // TODO
    KZ: 'Kazakhstan', // TODO
    LA: 'Lao People\'s Democratic Republic', // TODO
    LB: 'Libanon',
    LC: 'Saint Lucia',
    LI: 'Liechtenstein',
    LK: 'Sri Lanka',
    LR: 'Liberia',
    LS: 'Lesotho',
    LT: 'Lithauen',
    LU: 'Luxembourg',
    LV: 'Lettland',
    LY: 'Libyan Arab Jamahiriya', // TODO
    MA: 'Marokko',
    MC: 'Monaco',
    MD: 'Moldovien',
    ME: 'Montenegro',
    MF: 'Saint Martin',
    MG: 'Madagaskar',
    MH: 'Marshall Inseln',
    MK: 'Macedonia, The Former Yugoslav Republic of', // TODO
    ML: 'Mali',
    MM: 'Myanmar',
    MN: 'Mongolien',
    // TODO
    MO: 'Macao, Special Administrative Region of China',
    MP: 'Northern Mariana Islands, Commonwealth of the',
    MQ: 'Martinique',
    MR: 'Mauritania, Islamic Republic of',
    MS: 'Montserrat',
    MT: 'Malta',
    MU: 'Mauritius',
    MV: 'Maldives',
    MW: 'Malawi',
    MX: 'Mexiko',
    MY: 'Malaysia',
    MZ: 'Mozambique',
    NA: 'Namibia',
    NC: 'Neu Kaledonien',
    NE: 'Niger',
    NF: 'Norfolk Island',
    NG: 'Nigeria',
    NI: 'Nicaragua',
    NL: 'Niederlande',
    NO: 'Norwegen',
    NP: 'Nepal',
    NR: 'Nauru',
    NU: 'Niue',
    NZ: 'Neuseeland',
    OM: 'Oman',
    PA: 'Panama',
    PE: 'Peru',
    PF: 'French Polynesia',
    PG: 'Papua New Guinea, Independent State of',
    PH: 'Philippinen',
    PK: 'Pakistan',
    PL: 'Polen',
    PM: 'Saint Pierre and Miquelon',
    PN: 'Pitcairn Islands',
    PR: 'Puerto Rico',
    PS: 'Palestinian Territory, Occupied',
    PT: 'Portugal',
    PW: 'Palau',
    PY: 'Paraguay',
    QA: 'Qatar',
    RE: 'Reunion',
    RO: 'Rumänien',
    RS: 'Serbien',
    RU: 'Russland',
    RW: 'Rwanda',
    SA: 'Saudi Arabien',
    SB: 'Solomon Islands',
    SC: 'Seychelles',
    SD: 'Sudan',
    SE: 'Schweden',
    SG: 'Singapor',
    SH: 'Saint Helena',
    SI: 'Slowenien',
    SJ: 'Svalbard & Jan Mayen Islands',
    SK: 'Slowakei',
    SL: 'Sierra Leone',
    SM: 'San Marino',
    SN: 'Senegal',
    SO: 'Somalia',
    SR: 'Suriname',
    SS: 'South Sudan',
    ST: 'Sao Tome and Principe, Democratic Republic of',
    SV: 'El Salvador',
    SX: 'Sint Maarten (Niederlande)',
    SY: 'Syrian Arab Republic',
    SZ: 'Swaziland',
    TC: 'Turks and Caicos Islands',
    TD: 'Chad',
    TF: 'French Southern Territories',
    TG: 'Togo',
    TH: 'Thailand',
    TJ: 'Tajikistan',
    TK: 'Tokelau',
    TL: 'Timor-Leste',
    TM: 'Turkmenistan',
    TN: 'Tunesien',
    TO: 'Tonga',
    TR: 'Türkei',
    TT: 'Trinidad and Tobago',
    TV: 'Tuvalu',
    TW: 'Taiwan',
    TZ: 'Tanzania',
    UA: 'Ukraine',
    UG: 'Uganda',
    UM: 'United States Minor Outlying Islands',
    US: 'Vereinigte Staaten von Amerika',
    UY: 'Uruguay',
    UZ: 'Uzbekistan',
    VA: 'Vatikan-Stadt',
    VC: 'Saint Vincent and the Grenadines',
    VE: 'Venezuela',
    VG: 'British Virgin Islands',
    VI: 'United States Virgin Islands',
    VN: 'Vietnam',
    VU: 'Vanuatu',
    WF: 'Wallis and Futuna',
    WS: 'Samoa',
    XD: 'United Nations Neutral Zone',
    XE: 'Iraq-Saudi Arabia Neutral Zone',
    XS: 'Spratly Islands',
    XX: 'Disputed Territory',
    YE: 'Yemen',
    YT: 'Mayotte',
    ZA: 'Südafrika',
    ZM: 'Zambia',
    ZW: 'Zimbabwe'
  },

  '#': {
    admin: 'Administration',
    filter: 'Filter',
    home: 'Startseite',
    'loading-text': 'Läd...',
    login: 'Anmelden',
    logout: 'Abmelden',
    new: 'Neu',
    'no-data-text': 'Nichts anzuzeigen',
    'no-results-text': 'Nix gefunden',
    image: {
      by: 'Bild von',
      on: 'auf'
    }
  },

  '!': {
    'close-window': 'Verwirf die Eingaben und schließe den Dialog.'
  },

  contacts: {
    add: 'Neu',
    added: 'Angelegt',
    email: 'E-Mail',
    name: 'Name',
    notes: 'Notizen',
    removed: 'Gelöscht',
    status: 'Status',
    '!': {
      add: 'Weiteren Kontakt hinzufügen.'
    }
  },

  copyright: {
    back: 'Zurück'
  },

  countries: {
    count: '0 Länder | 1 Land | {count} Länder',
    servers: '{count} Server'
  },

  create: {
    abort: 'Abbrechen',
    contacts: 'Kontakte',
    protocols: 'Protokolle',
    save: 'Speichern',
    title: 'Neuen Server einrichten'
  },

  error: {
    err404: 'Seite nicht gefunden',
    other: 'Es ist ein Fehler aufgetreten'
  },

  license: {
    title: 'Lizenz'
  },

  location: {
    'ip-address': 'IP-Adresse',
    loading: 'Läd...',
    location: 'Ort',
    unknown: `Wir konnten deinen Standort nicht bestimmen.
      Das bedeutet, dass wir keinen Server in deiner Region anbieten können.
      Jeder Server, der dir angeboten wird, ist zufällig aus allen zur Verfügung
      stehenden Servern ausgewählt.`
  },

  login: {
    account: 'Kennung',
    cancel: 'Abbrechen',
    failed: 'Der Anmeldeversuch ist fehlgeschlagen.',
    passwd: 'Passwort',
    submit: 'Anmelden',
    title: 'Anmeldung zur Administration'
  },

  protocols: {
    added: 'Angelegt',
    notes: 'Notizen',
    path: 'Pfad',
    protocol: 'Protokoll',
    removed: 'Gelöscht',
    status: 'Status',
    '!': {
      add: 'Weiteres Protokoll hinzufügen.'
    }
  },

  server: {
    country: 'Land',
    name: 'DNS-Name',
    notes: 'Notizen',
    region: 'Region',
    status: 'Status',
    title: 'Spiegel',
    town: 'Stadt',
    list: {
      title: 'Server-Liste',
      country: 'Land',
      name: 'Name',
      segment: 'Segment',
      status: 'Status'
    },
    message: {
      404: 'Server wurde nicht gefunden',
      504: 'Back-end ist nicht erreichbar'
    },
    summary: {
      title: 'Summary'
    },
    tab: {
      access: 'Zugriff',
      contacts: 'Kontakte',
      monitor: 'Überwachung',
      server: 'Server'
    }
  },

  stat: {
    tab: {
      age: 'Alter',
      countries: 'Länder',
      location: 'Verortung'
    }
  }
}
