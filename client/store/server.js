/*
 * (c) 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */

export default {
  state: () => ({
    all: [],
    seq: -1
  }),

  mutations: {
    /**
     * Setter for all servers.
     */
    setAllServers (state, list) {
      state.all = list
    },

    /**
     * Setter for a server.
     */
    set (state, server) {
      state[server.id] = server
    },

    /**
     *
     */
    update (state, arg) {
      state[arg.id][arg.key] = arg.value
      // console.log('-------------------------- ' + arg.id + ' ' + arg.key + ' = ' + arg.value)
    },

    /**
     * Adder for a contact.
     */
    addContact (state, id) {
      state[id].contact.push({
        id: state.seq--,
        name: '',
        email: '',
        status: 'INACTIVE',
        dateAdded: new Date(),
        dateRemoved: '',
        notes: ''
      })
    },

    /**
     * Adder for an address.
     */
    addAddress (state, id) {
      state[id].addresses.push({
        id: state.seq--,
        protocol: 'HTTPS',
        path: '',
        status: 'INACTIVE',
        dateAdded: new Date(),
        dateRemoved: '',
        notes: ''
      })
    },

    /**
     * Deleter for an address.
     */
    deleteAddress (state, params) {
      const sid = params.sid
      const id = params.id
      if (state[sid] === undefined) {
        return
      }
      state[sid].addresses = state[sid].addresses.filter(item => item.id !== id)
    },

    /**
     * Deleter for a contact.
     */
    deleteContact (state, params) {
      const sid = params.sid
      const id = params.id
      if (state[sid] === undefined) {
        return
      }
      state[sid].contacts = state[sid].contacts.filter(item => item.id !== id)
    }
  },

  actions: {
    /**
     *
     */
    loadAll ({ commit, state }) {
      if (state.all.length === 0) {
        // console.log('################################################')
        // return
      }
      this.$axios.post('/api/server', { filter: '' })
        .then((result) => {
          commit('setAllServers', result.data)
        })
    },

    /**
     *
     */
    async load ({ commit, state }, id) {
      const result = await this.$axios.get('/api/server/' + id)
      commit('set', result.data)
    },

    /**
     *
     */
    update ({ commit, state }, arg) {
      this.$axios.put('/api/server/update', arg)
        .then((result) => {
          commit('update', arg)
        })
    }
  }
}
