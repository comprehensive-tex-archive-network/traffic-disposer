/*
** Copyright (c) 2019-2020  The CTAN Team and individual authors
**
** This file is distributed under the 3-clause BSD license.
** See file LICENSE for details.
*/
import cookies from 'vue-cookies'
import axios from 'axios'

/**
 * This is the vuex store for user-related information.
 *
 * @author Gerd Neugebauer <gene@ctan.org>
 */
export default {
  state: () => ({
    account: undefined,
    jwt: undefined,
    name: undefined
  }),

  mutations: {
    /**
     * Set the status to the arguments.
     */
    set (state, data) {
      state.name = data.name
      state.account = data.account
      state.jwt = data.jwt

      if (data.name !== undefined) {
        axios.defaults.headers.common.Authorization = 'Bearer ' + data.jwt
        cookies.set('jwt', data.jwt, '30d')
        cookies.set('user', data.account + '\t' + data.name, '30d')
      } else {
        axios.defaults.headers.common.Authorization = undefined
        cookies.remove('jwt')
        cookies.remove('user')
      }
    },

    /**
     *
     *
     */
    refreshLogin (state, callback) {
      const token = cookies.get('user')
      if (token === undefined || token == null) {
        state.name = undefined
        if (callback !== undefined) {
          callback(undefined)
        }
        return
      }

      const a = token.split('\t')
      state.account = a[0]
      state.name = a[1]
      state.jwt = cookies.get('jwt')
      axios.defaults.headers.common.Authorization = 'Bearer ' + state.jwt
      if (callback !== undefined) {
        callback(state)
      }
    }
  },

  actions: {
    /**
     * Attempt an asynchronous login on the Server and invoke the callback to
     * pass on the result.
     * <p>
     * The callback is invoked with undefined if the login attempt failed.
     *
     * @param payload.user the user name
     * @param payload.passwd the password
     * @param payload.callback the Callback, to complete the operation
     */
    async login ({ state, commit }, payload) {
      const callback = payload.callback
      state.account = undefined
      cookies.remove('jwt')
      cookies.remove('user')

      await axios.post('/api/login', {
        account: payload.account,
        passwd: payload.passwd
      })
        .then((response) => {
          const data = response.data
          if (data.name == null || data.jwt === undefined) {
            if (callback !== undefined) {
              callback(undefined, response.status)
            }
          } else {
            data.account = payload.account
            commit('set', data)

            if (callback !== undefined) {
              callback(state, 200)
            }
          }
        })
        .catch((error) => {
          if (callback !== undefined) {
            callback(undefined,
              error.response !== undefined ? error.response.status : 500)
          }
        })
    },

    /**
     *
     */
    logout ({ state, commit }) {
      commit('set', {})
    }
  },

  getters: {
    /**
     * Check that a user is currently logged in.
     */
    isAuthenticated: state => state.account !== undefined
  }
}
