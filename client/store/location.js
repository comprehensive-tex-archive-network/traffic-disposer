/*
** Copyright (c) 2020  The CTAN Team and individual authors
**
** This file is distributed under the 3-clause BSD license.
** See file LICENSE for details.
*/
import axios from 'axios'

/**
 * This is the vuex store for location-related information.
 *
 * @author Gerd Neugebauer <gene@ctan.org>
 */
export default {
  state: () => ({
    anonymous: true,
    loaded: false
  }),

  mutations: {
    /**
     * Set the status to the arguments.
     */
    set (state, data) {
      ['anonymous', 'city', 'continent', 'country', 'ipAddress', 'postal']
        .forEach((x) => {
          state[x] = data[x]
        })
      state.loaded = true
    }
  },

  actions: {
    /**
     *
     */
    load ({ commit, state }) {
      if (state.loaded) {
        return
      }

      axios.get('/api/location')
        .then((response) => {
          commit('set', response.data)
        })
    }
  }
}
