/*
 * (c) 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */

export default {
  state: () => ({
    byCountry: {},
    loaded: false
  }),

  mutations: {
    /**
     * Setter for all servers by country.
     */
    set (state, arg) {
      state.byCountry = arg
      state.loaded = new Date()
    }
  },

  actions: {
    /**
     *
     */
    async load ({ commit, state }) {
      if (state.loaded) {
        return
      }
      const result = await this.$axios.get('/api/stat/countries')
      commit('set', result.data)
    }
  }
}
