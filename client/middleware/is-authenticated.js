/*
** Copyright (c) 2019-2020  The CTAN Team and individual authors
**
** This file is distributed under the 3-clause BSD license.
** See file LICENSE for details.
*/
/**
 * This is the middleware component to check whether the currently authenticated
 * user is logged-in.
 *
 * @author Gerd Neugebauer <gene@ctan.org>
 */
export default function ({ store, redirect }) {
  if (store.state.user.name) {
    return
  }
  store.commit('user/refreshLogin')

  if (store.state.user.name) {
    return
  }
  return redirect('/login')
}
