/*
** Copyright (c) 2019-2020  The CTAN Team and individual authors
**
** This file is distributed under the 3-clause BSD license.
** See file LICENSE for details.
*/
/**
 * This is the middleware component to check whether the currently authenticated
 * user is allowed to access admin pages.
 *
 * @author Gerd Neugebauer <gene@ctan.org>
 */
export default function ({ store, redirect, next }) {
  if (!store.state.user.account) {
    return redirect('/login')
  }
  if (!store.getters['user/rolesAllowed']('ADMIN')) {
    next({ statusCode: 404, message: 'not found' })
  }
}
