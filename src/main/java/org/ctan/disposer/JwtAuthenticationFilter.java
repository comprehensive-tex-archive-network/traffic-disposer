/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */

package org.ctan.disposer;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.User;
import org.ctan.disposer.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Data;

/**
 * The class <code>JwtAuthenticationFilter</code> contains...TODO
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
public class JwtAuthenticationFilter
        extends UsernamePasswordAuthenticationFilter {

    @Data
    protected class ApplicationUser {

        private String username;

        private String password;
    }

    /**
     * The field <code>authenticationManager</code> contains the authentication
     * manager.
     */
    private AuthenticationManager authenticationManager;

    private AuthService authService;

    /**
     * This is the constructor for the <code>JwtAuthenticationFilter</code>.
     *
     * @param authManager the authentication manager
     */
    @Autowired
    public JwtAuthenticationFilter(AuthenticationManager authManager,
            AuthService authService) {

        this.authenticationManager = authManager;
        this.authService = authService;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.security.web.authentication.
     * UsernamePasswordAuthenticationFilter#attemptAuthentication(javax.servlet.
     * http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest req,
            HttpServletResponse res) {

        ApplicationUser creds;
        try {
            creds = new ObjectMapper()
                    .readValue(req.getInputStream(), ApplicationUser.class);
        } catch (IOException e) {
            throw new RuntimeException("IOException", e);
        }
        return authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(creds.getUsername(),
                        creds.getPassword(), new ArrayList<>()));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.security.web.authentication.
     * AbstractAuthenticationProcessingFilter#successfulAuthentication(javax.
     * servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse,
     * javax.servlet.FilterChain,
     * org.springframework.security.core.Authentication)
     */
    @Override
    protected void successfulAuthentication(HttpServletRequest req,
            HttpServletResponse res, FilterChain chain, Authentication auth)
            throws IOException, ServletException {

        String token = authService
                .createJwt(((User) auth.getPrincipal()).getUsername());
        res.addHeader("Authentication", "Bearer " + token);
    }

}
