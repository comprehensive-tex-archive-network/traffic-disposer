/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */
package org.ctan.disposer.controller;

import javax.servlet.http.HttpServletRequest;

import org.ctan.disposer.service.GeoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The class <code>RedirectController</code> contains the controller for
 * redirecting a request to a proper server.
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
@RestController
@RequestMapping(path = "/forward")
public class RedirectController {

    /**
     * The field <code>geoService</code> contains the Geo service.
     */
    private GeoService geoService;

    /**
     * This is the constructor for the class <code>RedirectController</code>.
     *
     * @param geoService the Geo service
     */
    @Autowired
    public RedirectController(GeoService geoService) {

        this.geoService = geoService;
    }

    /**
     * The method <code>redirect</code> is the redirecting method.
     *
     * @param request the request
     * 
     * @return the redirect URL
     */
    @GetMapping("**")
    public String redirect(HttpServletRequest request) {

        return "redirect:" + geoService.redirectUrl(request.getScheme(),
                request.getRemoteAddr());
    }
}
