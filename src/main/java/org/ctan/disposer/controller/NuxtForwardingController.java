/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */

package org.ctan.disposer.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * The class <code>NuxtForwardingController</code> forwards anything unknown to
 * <code>index.html</code> to be processed by Nuxt.
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
@Controller
public class NuxtForwardingController implements ErrorController {

    /**
     * The method <code>index</code> forwards errors to nuxt.
     *
     * @return the entry point
     */
    @RequestMapping("/error")
    public String index() {
        return "/index.html";
    }

    /**
     * Getter for the error path.
     */
    @Override
    public String getErrorPath() {
        return "/index.html";
    }
}
