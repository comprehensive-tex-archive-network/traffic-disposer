/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */
package org.ctan.disposer.controller;

import javax.servlet.http.HttpServletRequest;

import org.ctan.disposer.service.GeoService;
import org.ctan.disposer.service.GeoService.LocationData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The class <code>LocationController</code> is a controller for locating the
 * requester.
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
@RestController
@RequestMapping(path = "/api/location")
public class LocationController {

    /**
     * The field <code>geoService</code> contains the Geo service.
     */
    private GeoService geoService;

    /**
     * This is the constructor for the class <code>RedirectController</code>.
     *
     * @param geoService the Geo service
     */
    @Autowired
    public LocationController(GeoService geoService) {

        this.geoService = geoService;
    }

    /**
     * The method <code>whereAmI</code> provides means to determine the location
     * of a requester.
     *
     * @param request the request
     * 
     * @return the redirect URL
     */
    @GetMapping
    public LocationData whereAmI(HttpServletRequest request) {

        return geoService.location(request.getScheme(),
                request.getRemoteAddr());
    }
}
