/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */
package org.ctan.disposer.controller;

import org.ctan.disposer.service.MonitorService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The class <code>MonitorController</code> is the controller for monitor
 * services.
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
@RestController
@RequestMapping(path = "/api/monitor")
public class MonitorController {

    /**
     * The field <code>service</code> contains the service.
     */
    private MonitorService service;

    /**
     * This is the constructor for the class <code>MonitorController</code>.
     *
     * @param service the monitor service
     */
    public MonitorController(MonitorService service) {

        this.service = service;
    }

    /**
     * The method <code>update</code> triggers a probe for all servers.
     */
    @GetMapping("update")
    public void update() {

        service.probe();
    }

    /**
     * The method <code>updateOne</code> triggers a probe for a single mirror.
     *
     * @param id the id of the mirror to be probed
     */
    @GetMapping("update/{id}")
    public void updateOne(@PathVariable("id") Long id) {

        service.probe(id);
    }
}
