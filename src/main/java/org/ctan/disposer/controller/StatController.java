/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */
package org.ctan.disposer.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ctan.disposer.model.Country;
import org.ctan.disposer.model.Segment;
import org.ctan.disposer.model.Server;
import org.ctan.disposer.repository.ServerRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The class <code>StatController</code> contains the controller end-points to
 * retrieve statistical data.
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
@RestController
@RequestMapping(path = "/api/stat")
public class StatController {

    /**
     * The class <code>ServerData</code> is the transport object for server
     * data.
     */
    @Data
    @AllArgsConstructor
    private static class ServerData {

        private String name;
    }

    @Data
    @NoArgsConstructor
    private static class CountryStat {

        private int active = 0;
        private int inactive = 0;
        private List<ServerData> serverList = new ArrayList<>();

        /**
         * The method <code>add</code> provides means to add another server.
         *
         * @param server the server
         * @return <code>true</code> upon success
         */
        public boolean add(Server server) {

            return serverList.add(new ServerData(server.getName()));
        }

    }

    /**
     * The field <code>servers</code> contains the servers repository.
     */
    private ServerRepository servers;

    /**
     * This is the constructor for the class <code>MirrorController</code>.
     *
     * @param servers the servers repository
     */
    public StatController(ServerRepository servers) {

        this.servers = servers;
    }

    /**
     * The method <code>countries</code> is the end-point to retrieve the
     * countries.
     *
     * @return the countries
     */
    @GetMapping("countries")
    public Map<String, Map<Country, CountryStat>> countries() {

        Map<String, Map<Country, CountryStat>> stat = new HashMap<>();

        for (Server serv : servers.findAll()) {
            Country country = serv.getCountry();
            Segment segment = country.getSegment();
            switch (serv.getStatus()) {
            case ACTIVE:
                add(stat, serv, segment.toString(), country, true);
                break;
            case INACTIVE:
                add(stat, serv, segment.toString(), country, false);
                break;
            case MASTER:
                add(stat, serv, "/", serv.getCountry(), true);
                break;
            case PARTIAL:
                break;
            case WITHDRAWN:
                break;
            default:
                break;
            }
        }

        return stat;
    }

    /**
     * The method <code>add</code> provides means to add an item to the
     * statistical data.
     *
     * @param stat    the statistical data container
     * @param serv    the server
     * @param region  the region
     * @param country the country
     * @param active  the indicator for activity
     */
    private void add(Map<String, Map<Country, CountryStat>> stat, Server serv,
            String region, Country country, boolean active) {

        Map<Country, CountryStat> reg = stat.get(region);
        if (reg == null) {
            reg = new HashMap<>();
            stat.put(region, reg);
        }
        CountryStat co = reg.get(country);
        if (co == null) {
            co = new CountryStat();
            reg.put(country, co);
        }

        if (active) {
            co.active++;
            co.add(serv);
        } else {
            co.inactive++;
        }
    }
}
