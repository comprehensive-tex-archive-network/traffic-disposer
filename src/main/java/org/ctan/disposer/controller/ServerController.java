/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */
package org.ctan.disposer.controller;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.ctan.disposer.model.Country;
import org.ctan.disposer.model.Server;
import org.ctan.disposer.repository.ServerRepository;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The class <code>MirrorController</code> is the controller for requesting
 * servers.
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
@RestController
@RequestMapping(path = "/api/server")
public class ServerController {

    // public static Specification<Mirror> nameContains(String term) {
    // List<Predicate> predicates = new ArrayList<>();
    // return (root, query, builder) -> //
    // builder.or(like(root.get("name"), term) ,contains(term));
    // }

    /**
     * The method <code>noteContains</code> provides means to check for a
     * substring in the note field.
     *
     * @param expression the expression
     * @return the specification
     */
    private static Specification<Server> noteContains(String expression) {

        return (root, query, builder) -> builder.like(root.get("note"),
                contains(expression));
    }

    /**
     * The method <code>nameContains</code> provides means to check for a
     * substring in the name field.
     *
     * @param expression the expression
     * @return the specification
     */
    private static Specification<Server> nameContains(String expression) {

        return (root, query, builder) -> builder.like(root.get("name"),
                contains(expression));
    }

    /**
     * The method <code>countryContains</code> provides means to check for a
     * substring in the country field.
     *
     * @param expression the expression
     * @return the specification
     */
    private static Specification<Server> countryContains(String expression) {

        return (root, query, builder) -> builder.like(root.get("country"),
                contains(expression));
    }

    /**
     * The method <code>contains</code> packages an expression as substring
     * pattern.
     *
     * @param expression the expression
     * @return the specification
     */
    private static String contains(String expression) {

        return MessageFormat.format("%{0}%", expression);
    }

    /**
     * The field <code>servers</code> contains the servers repository.
     */
    private ServerRepository servers;

    /**
     * This is the constructor for the class <code>MirrorController</code>.
     *
     * @param servers the servers repository
     */
    public ServerController(ServerRepository servers) {

        this.servers = servers;
    }

    /**
     * The method <code>findById</code> provides means to retrieve a single
     * mirror by its id.
     *
     * @param id the id
     * @return the mirror as Map or an exception is thrown
     */
    @GetMapping("{id}")
    public Map<String, Object> findById(@PathVariable("id") Long id) {

        Optional<Server> optMirror = servers.findById(id);
        if (optMirror.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "mirror not found");
        }

        Server mirror = optMirror.get();

        Map<String, Object> instance = new HashMap<>();
        instance.put("id", mirror.getId());
        instance.put("name", mirror.getName());
        instance.put("notes", mirror.getNotes());
        Country country = mirror.getCountry();
        instance.put("country", country);
        instance.put("segment", country.getSegment());
        instance.put("region", mirror.getRegion());
        instance.put("town", mirror.getTown());
        instance.put("status", mirror.getStatus());
        instance.put("contacts", mirror.getContacts());
        instance.put("addresses", mirror.getAddresses());
        return instance;
    }

    /**
     * The method <code>findAll</code> provides he end-point for retrieving all
     * servers.
     *
     * @return the servers
     */
    @PostMapping
    public List<Map<String, Object>> findAll() {

        String filter = null; // fixme
        List<Map<String, Object>> all = new ArrayList<>();
        if (filter == null || filter.isEmpty()) {
            all = servers.findAll(Sort.by("country", "name")).stream()
                    .map(mirror -> {
                        Map<String, Object> instance = new HashMap<>();
                        instance.put("id", mirror.getId());
                        instance.put("name", mirror.getName());
                        Country country = mirror.getCountry();
                        instance.put("country", country);
                        instance.put("segment", country.getSegment());
                        instance.put("region", mirror.getRegion());
                        instance.put("town", mirror.getTown());
                        instance.put("status", mirror.getStatus());
                        return instance;
                    }).collect(Collectors.toList());
        } else {

            Specification<Server> specification = Specification
                    .where(nameContains(filter)).or(countryContains(filter))
                    .or(noteContains(filter));

            servers.findAll(specification, Sort.by("country", "name"));
        }
        return all;
    }

    /**
     * The class <code>UpdateParams</code> is the transport object for the
     * update data.
     */
    @Data
    @NoArgsConstructor
    public static class UpdateParams {
        private long id;
        private String key;
        private String value;
    }

    /**
     * The method <code>update</code> provides means to update a single
     * attribute.
     *
     * @param params the update parameter
     * @return
     */
    @PutMapping("update")
    public boolean update(@RequestBody UpdateParams params) {

        Optional<Server> om = servers.findById(params.getId());
        if (om.isEmpty()) {
            // TODO
            return false;
        }
        Server m = om.get();
        switch (params.getKey()) {
        case "name":
            m.setName(params.getValue());
            break;
        case "region":
            m.setRegion(params.getValue());
            break;
        case "status":
            m.setStatus(Server.Status.valueOf(params.getValue()));
            break;
        case "town":
            m.setTown(params.getValue());
            break;
        case "notes":
            m.setNotes(params.getValue());
            break;
        default:
            throw new IllegalArgumentException(params.getKey());
        }
        servers.save(m);
        return true;
    }
}
