/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */
package org.ctan.disposer.controller;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import org.ctan.disposer.model.Address;
import org.ctan.disposer.model.Probe.Status;
import org.ctan.disposer.model.Server;
import org.ctan.disposer.repository.ServerRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * The class <code>ProbeController</code> contains the controller methods for
 * probing.
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
@RestController
@RequestMapping(path = "/api/probe")
public class ProbeController {

    /**
     * The field <code>servers</code> contains the servers repository.
     */
    private ServerRepository servers;

    /**
     * This is the constructor for the class <code>ProbeController</code>.
     *
     * @param servers the servers repository
     */
    public ProbeController(ServerRepository servers) {

        this.servers = servers;
    }

    /**
     * The class <code>ProbeData</code> contains the transport object for probe
     * data.
     */
    @Data
    @AllArgsConstructor
    @Builder
    public static class ProbeData {

        /**
         * The field <code>date</code> contains the date and time of the probe.
         */
        @Column
        private LocalDateTime time;

        /**
         * The field <code>status</code> contains the status of the probe.
         */
        @Column
        @Enumerated(EnumType.STRING)
        private Status status;
    }

    /**
     * The method <code>countries</code> provides means to retrieve probe data
     * for one server.
     *
     * @return the probe data structured by protocol
     */
    @GetMapping("for/{id}")
    public Map<String, List<ProbeData>> forServer(@PathVariable long id) {

        Server m = servers.getOne(id);
        Map<String, List<ProbeData>> result = new HashMap<>();

        for (Address addr : m.getAddresses()) {
            result.put(addr.getProtocol().name(), //
                    addr.getProbes().stream() //
                            .map((p) -> {
                                return ProbeData.builder() //
                                        .time(p.getTime()) //
                                        .status(p.getStatus()) //
                                        .build();
                            }) //
                            .collect(Collectors.toList()));
        }

        return result;
    }

}
