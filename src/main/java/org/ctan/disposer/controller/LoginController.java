/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */
package org.ctan.disposer.controller;

import java.util.Map;

import javax.annotation.security.PermitAll;

import org.ctan.disposer.service.AuthService;
import org.ctan.disposer.service.AuthService.AuthUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * The class <code>LoginController</code> is the controller for user login.
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
@RestController
@RequestMapping(path = "/api/login")
@Slf4j
public class LoginController {

    /**
     * The class <code>LoginData</code> is the transport object for login data.
     */
    @Data
    @NoArgsConstructor
    private static class LoginData {
        private String account;
        private String passwd;
    }

    /**
     * The field <code>auth</code> contains the auth service.
     */
    private AuthService auth;

    /**
     * This is the constructor for the class <code>LoginController</code>.
     *
     * @param auth the auth service
     */
    @Autowired
    public LoginController(AuthService auth) {

        this.auth = auth;
    }

    /**
     * The method <code>login</code> provides means to login.
     *
     * @param params the login form
     * @return
     */
    @PostMapping()
    @PermitAll
    public Map<String, String> login(@RequestBody LoginData params) {

        String account = params.getAccount();
        String passwd = params.getPasswd();
        AuthUserDetails user;

        if (account == null || passwd == null
                || (user = auth.verifyPassword(account, passwd)) == null
                || !user.isAccountNonLocked()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        return Map.of("name", user.getName(), //
                "jwt", auth.createJwt(account));
    }
}
