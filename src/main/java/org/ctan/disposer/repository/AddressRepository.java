/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */

package org.ctan.disposer.repository;

import java.util.List;

import org.ctan.disposer.model.Address;
import org.ctan.disposer.model.Address.Status;
import org.ctan.disposer.model.Country;
import org.ctan.disposer.model.Protocol;
import org.ctan.disposer.model.Server;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * The class <code>AddressRepository</code> describes the access to the database
 * of addresses.
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {

    /**
     * The method <code>findAllByProtocolAndStatus</code> provides means to find
     * the list of addresses for a given protocol and status.
     *
     * @param protocol the protocol
     * @param status   the status
     * @param active
     * @return the matching list
     */
    @Query("select ma from Address ma where " //
            + "ma.protocol = :protocol and " //
            + "ma.status = :status and " //
            + "ma.server.status = :serverStatus")
    List<Address> findAllByProtocolAndStatus(
            @Param("protocol") Protocol protocol, //
            @Param("status") Address.Status status, //
            @Param("serverStatus") Server.Status serverStatus);

    /**
     * The method <code>findAllByProtocolAndStatusAndCountry</code> provides
     * means to find the list of addresses for a given protocol, status, and
     * country.
     *
     * @param protocol     the protocol
     * @param status       the status
     * @param serverStatus the server status
     * @param country      the country
     * @return the matching list
     */
    @Query("select ma from Address ma where " //
            + "ma.protocol = :protocol and " //
            + "ma.status = :status and " //
            + "ma.server.status = :serverStatus and " //
            + "ma.server.country = :country")
    List<Address> findAllByProtocolAndStatusAndCountry(
            @Param("protocol") Protocol protocol,
            @Param("status") Status status,
            @Param("serverStatus") Server.Status serverStatus,
            @Param("country") Country country);

    /**
     * The method <code>findAllByMirrorId</code> provides means to find the list
     * of addresses for a given server id.
     *
     * @param id the id
     * @return the matching list
     */
    @Query("select ma from Address ma where " //
            + "ma.server.id = :id")
    List<Address> findAllByServerId(@Param("id") Long id);

}
