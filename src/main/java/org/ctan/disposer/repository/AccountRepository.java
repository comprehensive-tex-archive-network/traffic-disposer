/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */

package org.ctan.disposer.repository;

import org.ctan.disposer.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The class <code>AccountRepository</code> describes the access to the database
 * for user accounts.
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

    /**
     * The method <code>findByAccount</code> provides means to retrieve an
     * account by the user name.
     *
     * @param account the user name
     * @return the account or <code>null</code>
     */
    Account findByAccount(String account);
}
