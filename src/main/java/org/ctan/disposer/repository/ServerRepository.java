/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */

package org.ctan.disposer.repository;

import java.util.List;

import org.ctan.disposer.model.Country;
import org.ctan.disposer.model.Server;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * The class <code>ServerRepository</code> describes the access to the database.
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
@Repository
public interface ServerRepository
        extends JpaRepository<Server, Long>, JpaSpecificationExecutor<Server> {

    /**
     * The method <code>findAllByStatusAndCountry</code> provides means to find
     * all servers with a given status and country.
     *
     * @param status the status
     * @param country the country
     * @return the list of matching servers
     */
    List<Server> findAllByStatusAndCountry(Server.Status status,
            Country country);
}
