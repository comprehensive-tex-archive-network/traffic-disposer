/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */
package org.ctan.disposer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * The class <code>TrafficDisposer</code> contains the command line interface
 * for the Traffic Disposer. It starts a Spring Boot application for the
 * back-end server.
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
@SpringBootApplication
@EnableScheduling
public class TrafficDisposer {

    /**
     * The method <code>main</code> contains the command line interface to start
     * the Spring Boot application.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        SpringApplication.run(TrafficDisposer.class, args);
    }

}
