/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */

package org.ctan.disposer.task;

import org.ctan.disposer.service.MonitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * The class <code>MonitorTask</code> is a task to monitor all active servers
 * in turn.
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
@Component
public class MonitorTask {

    /**
     * The field <code>monitor</code> contains the monitor service.
     */
    private MonitorService monitor;

    /**
     * This is the constructor for the class <code>MonitorTask</code>.
     *
     * @param monitor the monitor service
     */
    @Autowired
    public MonitorTask(MonitorService monitor) {

        this.monitor = monitor;
    }

    /**
     * The method <code>monitorAll</code> provides means to probe all servers.
     * It is scheduled once in an hour.
     */
    @Scheduled(cron = "0 30 * * * *")
    public void monitorAll() {

        monitor.probe();
    }

}
