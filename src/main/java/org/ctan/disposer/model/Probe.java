/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */

package org.ctan.disposer.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The class <code>MirrorProbe</code> represents a request to one mirror service
 * and the result.
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Probe {

    /**
     * The enumeration <code>Status</code> represents the outcomes of a probe.
     */
    public enum Status {
        OK, //
        NOT_FOUND, //
        NOT_REACHABLE, //
        ERROR
    }

    /**
     * The field <code>time</code> contains the date and time of the probe.
     */
    @Id
    @Column
    private LocalDateTime time;

    /**
     * The field <code>address</code> contains the address.
     */
    @ManyToOne
    @JsonIgnore
    private Address address;

    @Column
    private Long age;

    /**
     * The field <code>status</code> contains the status of the probe.
     */
    @Column
    @Enumerated(EnumType.STRING)
    private Status status;
}
