/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */

package org.ctan.disposer.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;

/**
 * The class <code>Role</code> contains an authority or right.
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
public class Role implements GrantedAuthority {

    /**
     * The field <code>ROLES</code> contains supported roles.
     */
    private static final Role[] ROLES = { new Role("ADMIN", 'A'),
            new Role("USER_ADMIN", 'U') };

    private static final Map<Character, Role> ROLES_MAP;
    static {
        ROLES_MAP = new HashMap<>();
        for (Role r : ROLES) {
            ROLES_MAP.put(r.code, r);
        }
    }

    /**
     * The method <code>compress</code> provides means to TODO gene
     *
     * @param roles
     * @return
     */
    public static String compress(Set<Role> roles) {

        return String.valueOf(roles.stream() //
                .map(r -> r.code) //
                .collect(Collectors.toList() //
                ).toArray());
    }

    /**
     * The method <code>of</code> provides means to translate short codes to an
     * enumeration element.
     *
     * @param roles the short code
     * @return the enumeration element
     */
    public static Set<Role> of(String roles) {

        Set<Role> result = new HashSet<>();

        for (char c : roles.toCharArray()) {
            Role r = ROLES_MAP.get(c);
            if (r != null) {
                result.add(r);
            }
        }
        return result;
    }

    /**
     * The field <code>code</code> contains the stored code.
     */
    private char code;

    /**
     * The field <code>name</code> contains the long name.
     */
    private String name;

    /**
     * This is the constructor for the class <code>Role</code>.
     *
     * @param code the code
     */
    private Role(String name, char code) {

        this.name = name;
        this.code = code;
    }

    /**
     * Getter for the long name.
     */
    @Override
    public String getAuthority() {

        return name;
    }
}
