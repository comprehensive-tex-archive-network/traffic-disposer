/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */

package org.ctan.disposer.model;

/**
 * The enumeration <code>Continent</code> represents a continent.
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
public enum Segment {

    /**
     * The field <code>AF</code> contains the value for Africa.
     */
    AF,
    /**
     * The field <code>AN</code> contains the value for Antarctica.
     */
    AN,
    /**
     * The field <code>AS</code> contains the value for Asia.
     */
    AS,
    /**
     * The field <code>OC</code> contains the value for Oceania.
     */
    OC,
    /**
     * The field <code>EU</code> contains the value for Europe.
     */
    EU,
    /**
     * The field <code>NA</code> contains the value for North America.
     */
    NA,
    /**
     * The field <code>SA</code> contains the value for South America.
     */
    SA
}
