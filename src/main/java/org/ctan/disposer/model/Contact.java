/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */

package org.ctan.disposer.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The class <code>Mirror</code> represents a personal contact for a server.
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Contact {

    /**
     * The class <code>ContactStatus</code> represents the status of the
     * contact.
     */
    public enum ContactStatus {
        ACTIVE, //
        INACTIVE, //
        WITHDRAWN
    }

    /**
     * The field <code>id</code> contains the unique id.
     */
    @Id
    @GeneratedValue
    private Long id;

    /**
     * The field <code>name</code> contains the name of the contact person.
     */
    @Column(length = 255)
    private String name;

    /**
     * The field <code>email</code> contains the email address of the contact
     * person.
     */
    @Column(length = 255)
    private String email;

    /**
     * The field <code>dateAdded</code> contains the optional date when the
     * contact person has been added.
     */
    @Column
    private LocalDateTime dateAdded;

    /**
     * The field <code>dateRemoved</code> contains the optional date when the
     * contact person has been withdrawn.
     */
    @Column
    private LocalDateTime dateRemoved;

    /**
     * The field <code>note</code> contains optional notes.
     */
    @Column(length = 4096)
    private String notes;

    /**
     * The field <code>status</code> contains the status of the contact.
     */
    @Column
    @Enumerated(EnumType.STRING)
    private ContactStatus status;

    /**
     * The field <code>mirror</code> contains the mirror to which this contacts
     * belongs to.
     */
    @ManyToOne
    @JsonIgnore
    private Server server;

}
