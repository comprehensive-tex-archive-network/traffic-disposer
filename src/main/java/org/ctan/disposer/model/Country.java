/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */

package org.ctan.disposer.model;

import lombok.Getter;

/**
 * The class <code>Country</code> represents a country.
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
@Getter
public enum Country {

    AD("Andorra, Principality of", Segment.EU), //
    AE("United Arab Emirates", Segment.AS), //
    AF("Afghanistan, Islamic Republic of", Segment.AS), //
    AG("Antigua and Barbuda", Segment.NA), //
    AI("Anguilla", Segment.NA), //
    AL("Albania, Republic of", Segment.EU), //
    AM("Armenia, Republic of", Segment.AS), // EU
    AN("Netherlands Antilles", Segment.NA), //
    AO("Angola, Republic of", Segment.AF), //
    AQ("Antarctica (the territory South of 60 deg S)", Segment.AN), //
    AR("Argentina, Argentine Republic", Segment.SA), //
    AS("American Samoa", Segment.OC), //
    AT("Austria, Republic of", Segment.EU), //
    AU("Australia, Commonwealth of", Segment.OC), //
    AW("Aruba", Segment.NA), //
    AX("Ã…land Islands", Segment.EU), //
    AZ("Azerbaijan, Republic of", Segment.AS), // EU
    BA("Bosnia and Herzegovina", Segment.EU), //
    BB("Barbados", Segment.NA), //
    BD("Bangladesh, People's Republic of", Segment.AS), //
    BE("Belgium, Kingdom of", Segment.EU), //
    BF("Burkina Faso", Segment.AF), //
    BG("Bulgaria, Republic of", Segment.EU), //
    BH("Bahrain, Kingdom of", Segment.AS), //
    BI("Burundi, Republic of", Segment.AF), //
    BJ("Benin, Republic of", Segment.AF), //
    BL("Saint Barthelemy", Segment.NA), //
    BM("Bermuda", Segment.NA), //
    BN("Brunei Darussalam", Segment.AS), //
    BO("Bolivia, Republic of", Segment.SA), //
    BQ("Bonaire, Sint Eustatius and Saba", Segment.NA), //
    BR("Brazil, Federative Republic of", Segment.SA), //
    BS("Bahamas, Commonwealth of the", Segment.NA), //
    BT("Bhutan, Kingdom of", Segment.AS), //
    BV("Bouvet Island (Bouvetoya)", Segment.AN), //
    BW("Botswana, Republic of", Segment.AF), //
    BY("Belarus, Republic of", Segment.EU), //
    BZ("Belize", Segment.NA), //
    CA("Canada", Segment.NA), //
    CC("Cocos (Keeling) Islands", Segment.AS), //
    CD("Congo, Democratic Republic of the", Segment.AF), //
    CF("Central African Republic", Segment.AF), //
    CG("Congo, Republic of the", Segment.AF), //
    CH("Switzerland, Swiss Confederation", Segment.EU), //
    CI("Cote d'Ivoire, Republic of", Segment.AF), //
    CK("Cook Islands", Segment.OC), //
    CL("Chile, Republic of", Segment.SA), //
    CM("Cameroon, Republic of", Segment.AF), //
    CN("China, People's Republic of", Segment.AS), //
    CO("Colombia, Republic of", Segment.SA), //
    CR("Costa Rica, Republic of", Segment.NA), //
    CU("Cuba, Republic of", Segment.NA), //
    CV("Cape Verde, Republic of", Segment.AF), //
    CW("CuraÃ§ao", Segment.NA), //
    CX("Christmas Island", Segment.AS), //
    CY("Cyprus, Republic of", Segment.EU), // AS
    CZ("Czech Republic", Segment.EU), //
    DE("Germany, Federal Republic of", Segment.EU), //
    DJ("Djibouti, Republic of", Segment.AF), //
    DK("Denmark, Kingdom of", Segment.EU), //
    DM("Dominica, Commonwealth of", Segment.NA), //
    DO("Dominican Republic", Segment.NA), //
    DZ("Algeria, People's Democratic Republic of", Segment.AF), //
    EC("Ecuador, Republic of", Segment.SA), //
    EE("Estonia, Republic of", Segment.EU), //
    EG("Egypt, Arab Republic of", Segment.AF), //
    EH("Western Sahara", Segment.AF), //
    ER("Eritrea, State of", Segment.AF), //
    ES("Spain, Kingdom of", Segment.EU), //
    ET("Ethiopia, Federal Democratic Republic of", Segment.AF), //
    FI("Finland, Republic of", Segment.EU), //
    FJ("Fiji, Republic of the Fiji Islands", Segment.OC), //
    FK("Falkland Islands (Malvinas)", Segment.SA), //
    FM("Micronesia, Federated States of", Segment.OC), //
    FO("Faroe Islands", Segment.EU), //
    FR("France, French Republic", Segment.EU), //
    GA("Gabon, Gabonese Republic", Segment.AF), //
    GB("United Kingdom of Great Britain & Northern Ireland", Segment.EU), //
    GD("Grenada", Segment.NA), //
    GE("Georgia", Segment.EU), // AS
    GF("French Guiana", Segment.SA), //
    GG("Guernsey, Bailiwick of", Segment.EU), //
    GH("Ghana, Republic of", Segment.AF), //
    GI("Gibraltar", Segment.EU), //
    GL("Greenland", Segment.NA), //
    GM("Gambia, Republic of the", Segment.AF), //
    GN("Guinea, Republic of", Segment.AF), //
    GP("Guadeloupe", Segment.NA), //
    GQ("Equatorial Guinea, Republic of", Segment.AF), //
    GR("Greece, Hellenic Republic", Segment.EU), //
    GS("South Georgia and the South Sandwich Islands", Segment.AN), //
    GT("Guatemala, Republic of", Segment.NA), //
    GU("Guam", Segment.OC), //
    GW("Guinea-Bissau, Republic of", Segment.AF), //
    GY("Guyana, Co-operative Republic of", Segment.SA), //
    HK("Hong Kong, Special Administrative Region of China", Segment.AS), //
    HM("Heard Island and McDonald Islands", Segment.AN), //
    HN("Honduras, Republic of", Segment.NA), //
    HR("Croatia, Republic of", Segment.EU), //
    HT("Haiti, Republic of", Segment.NA), //
    HU("Hungary, Republic of", Segment.EU), //
    ID("Indonesia, Republic of", Segment.AS), //
    IE("Ireland", Segment.EU), //
    IL("Israel, State of", Segment.AS), //
    IM("Isle of Man", Segment.EU), //
    IN("India, Republic of", Segment.AS), //
    IO("British Indian Ocean Territory (Chagos Archipelago)", Segment.AS), //
    IQ("Iraq, Republic of", Segment.AS), //
    IR("Iran, Islamic Republic of", Segment.AS), //
    IS("Iceland, Republic of", Segment.EU), //
    IT("Italy, Italian Republic", Segment.EU), //
    JE("Jersey, Bailiwick of", Segment.EU), //
    JM("Jamaica", Segment.NA), //
    JO("Jordan, Hashemite Kingdom of", Segment.AS), //
    JP("Japan", Segment.AS), //
    KE("Kenya, Republic of", Segment.AF), //
    KG("Kyrgyz Republic", Segment.AS), //
    KH("Cambodia, Kingdom of", Segment.AS), //
    KI("Kiribati, Republic of", Segment.OC), //
    KM("Comoros, Union of the", Segment.AF), //
    KN("Saint Kitts and Nevis, Federation of", Segment.NA), //
    KP("Korea, Democratic People's Republic of", Segment.AS), //
    KR("Korea, Republic of", Segment.AS), //
    KW("Kuwait, State of", Segment.AS), //
    KY("Cayman Islands", Segment.NA), //
    KZ("Kazakhstan, Republic of", Segment.AS), // EU
    LA("Lao People's Democratic Republic", Segment.AS), //
    LB("Lebanon, Lebanese Republic", Segment.AS), //
    LC("Saint Lucia", Segment.NA), //
    LI("Liechtenstein, Principality of", Segment.EU), //
    LK("Sri Lanka, Democratic Socialist Republic of", Segment.AS), //
    LR("Liberia, Republic of", Segment.AF), //
    LS("Lesotho, Kingdom of", Segment.AF), //
    LT("Lithuania, Republic of", Segment.EU), //
    LU("Luxembourg, Grand Duchy of", Segment.EU), //
    LV("Latvia, Republic of", Segment.EU), //
    LY("Libyan Arab Jamahiriya", Segment.AF), //
    MA("Morocco, Kingdom of", Segment.AF), //
    MC("Monaco, Principality of", Segment.EU), //
    MD("Moldova, Republic of", Segment.EU), //
    ME("Montenegro, Republic of", Segment.EU), //
    MF("Saint Martin", Segment.NA), //
    MG("Madagascar, Republic of", Segment.AF), //
    MH("Marshall Islands, Republic of the", Segment.OC), //
    MK("Macedonia, The Former Yugoslav Republic of", Segment.EU), //
    ML("Mali, Republic of", Segment.AF), //
    MM("Myanmar, Union of", Segment.AS), //
    MN("Mongolia", Segment.AS), //
    MO("Macao, Special Administrative Region of China", Segment.AS), //
    MP("Northern Mariana Islands, Commonwealth of the", Segment.OC), //
    MQ("Martinique", Segment.NA), //
    MR("Mauritania, Islamic Republic of", Segment.AF), //
    MS("Montserrat", Segment.NA), //
    MT("Malta, Republic of", Segment.EU), //
    MU("Mauritius, Republic of", Segment.AF), //
    MV("Maldives, Republic of", Segment.AS), //
    MW("Malawi, Republic of", Segment.AF), //
    MX("Mexico, United Mexican States", Segment.NA), //
    MY("Malaysia", Segment.AS), //
    MZ("Mozambique, Republic of", Segment.AF), //
    NA("Namibia, Republic of", Segment.AF), //
    NC("New Caledonia", Segment.OC), //
    NE("Niger, Republic of", Segment.AF), //
    NF("Norfolk Island", Segment.OC), //
    NG("Nigeria, Federal Republic of", Segment.AF), //
    NI("Nicaragua, Republic of", Segment.NA), //
    NL("Netherlands, Kingdom of the", Segment.EU), //
    NO("Norway, Kingdom of", Segment.EU), //
    NP("Nepal, State of", Segment.AS), //
    NR("Nauru, Republic of", Segment.OC), //
    NU("Niue", Segment.OC), //
    NZ("New Zealand", Segment.OC), //
    OM("Oman, Sultanate of", Segment.AS), //
    PA("Panama, Republic of", Segment.NA), //
    PE("Peru, Republic of", Segment.SA), //
    PF("French Polynesia", Segment.OC), //
    PG("Papua New Guinea, Independent State of", Segment.OC), //
    PH("Philippines, Republic of the", Segment.AS), //
    PK("Pakistan, Islamic Republic of", Segment.AS), //
    PL("Poland, Republic of", Segment.EU), //
    PM("Saint Pierre and Miquelon", Segment.NA), //
    PN("Pitcairn Islands", Segment.OC), //
    PR("Puerto Rico, Commonwealth of", Segment.NA), //
    PS("Palestinian Territory, Occupied", Segment.AS), //
    PT("Portugal, Portuguese Republic", Segment.EU), //
    PW("Palau, Republic of", Segment.OC), //
    PY("Paraguay, Republic of", Segment.SA), //
    QA("Qatar, State of", Segment.AS), //
    RE("Reunion", Segment.AF), //
    RO("Romania", Segment.EU), //
    RS("Serbia, Republic of", Segment.EU), //
    RU("Russian Federation", Segment.EU), // AS
    RW("Rwanda, Republic of", Segment.AF), //
    SA("Saudi Arabia, Kingdom of", Segment.AS), //
    SB("Solomon Islands", Segment.OC), //
    SC("Seychelles, Republic of", Segment.AF), //
    SD("Sudan, Republic of", Segment.AF), //
    SE("Sweden, Kingdom of", Segment.EU), //
    SG("Singapore, Republic of", Segment.AS), //
    SH("Saint Helena", Segment.AF), //
    SI("Slovenia, Republic of", Segment.EU), //
    SJ("Svalbard & Jan Mayen Islands", Segment.EU), //
    SK("Slovakia (Slovak Republic)", Segment.EU), //
    SL("Sierra Leone, Republic of", Segment.AF), //
    SM("San Marino, Republic of", Segment.EU), //
    SN("Senegal, Republic of", Segment.AF), //
    SO("Somalia, Somali Republic", Segment.AF), //
    SR("Suriname, Republic of", Segment.SA), //
    SS("South Sudan", Segment.AF), //
    ST("Sao Tome and Principe, Democratic Republic of", Segment.AF), //
    SV("El Salvador, Republic of", Segment.NA), //
    SX("Sint Maarten (Netherlands)", Segment.NA), //
    SY("Syrian Arab Republic", Segment.AS), //
    SZ("Swaziland, Kingdom of", Segment.AF), //
    TC("Turks and Caicos Islands", Segment.NA), //
    TD("Chad, Republic of", Segment.AF), //
    TF("French Southern Territories", Segment.AN), //
    TG("Togo, Togolese Republic", Segment.AF), //
    TH("Thailand, Kingdom of", Segment.AS), //
    TJ("Tajikistan, Republic of", Segment.AS), //
    TK("Tokelau", Segment.OC), //
    TL("Timor-Leste, Democratic Republic of", Segment.AS), //
    TM("Turkmenistan", Segment.AS), //
    TN("Tunisia, Tunisian Republic", Segment.AF), //
    TO("Tonga, Kingdom of", Segment.OC), //
    TR("Turkey, Republic of", Segment.EU), // AS
    TT("Trinidad and Tobago, Republic of", Segment.NA), //
    TV("Tuvalu", Segment.OC), //
    TW("Taiwan", Segment.AS), //
    TZ("Tanzania, United Republic of", Segment.AF), //
    UA("Ukraine", Segment.EU), //
    UG("Uganda, Republic of", Segment.AF), //
    UM("United States Minor Outlying Islands", Segment.NA), // OC
    US("United States of America", Segment.NA), //
    UY("Uruguay, Eastern Republic of", Segment.SA), //
    UZ("Uzbekistan, Republic of", Segment.AS), //
    VA("Holy See (Vatican City State)", Segment.EU), //
    VC("Saint Vincent and the Grenadines", Segment.NA), //
    VE("Venezuela, Bolivarian Republic of", Segment.SA), //
    VG("British Virgin Islands", Segment.NA), //
    VI("United States Virgin Islands", Segment.NA), //
    VN("Vietnam, Socialist Republic of", Segment.AS), //
    VU("Vanuatu, Republic of", Segment.OC), //
    WF("Wallis and Futuna", Segment.OC), //
    WS("Samoa, Independent State of", Segment.OC), //
    XD("United Nations Neutral Zone", Segment.AS), //
    XE("Iraq-Saudi Arabia Neutral Zone", Segment.AS), //
    XS("Spratly Islands", Segment.AS), //
    XX("Disputed Territory", Segment.OC), //
    YE("Yemen", Segment.AS), //
    YT("Mayotte", Segment.AF), //
    ZA("Africa, Republic of", Segment.AF), //
    ZM("Zambia, Republic of", Segment.AF), //
    ZW("Zimbabwe, Republic of", Segment.AF); //

    /**
     * The field <code>name</code> contains the name.
     */
    private String name;

    /**
     * The field <code>segment</code> contains the segment.
     */
    private Segment segment;

    /**
     * This is the constructor for the class <code>Country</code>.
     *
     * @param name the name
     * @param segment the segment
     */
    Country(String name, Segment segment) {

        this.name = name;
        this.segment = segment;
    }
}
