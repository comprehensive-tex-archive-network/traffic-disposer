/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */

package org.ctan.disposer.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The class <code>Address</code> represents an address on the server to be used
 * for downloading files.
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Address {

    /**
     * The class <code>ProbeSummary</code> contains the statistics cache.
     */
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Embeddable
    public static class ProbeSummary {
        private int count;
        private int active;
    }

    /**
     * The class <code>Status</code> represents the status of an address.
     */
    public enum Status {
        ACTIVE, //
        INACTIVE, //
        WITHDRAWN
    }

    /**
     * The field <code>id</code> contains the primary key.
     */
    @Id
    @GeneratedValue
    private Long id;

    /**
     * The field <code>mirror</code> contains the related mirror.
     */
    @ManyToOne
    @JoinColumn(name = "server_id", nullable = true)
    @JsonIgnore
    private Server server;

    /**
     * The field <code>protocol</code> contains the protocol.
     */
    @Column
    @Enumerated(EnumType.STRING)
    private Protocol protocol;

    /**
     * The field <code>path</code> contains the path of the address.
     */
    @Column(length = 1024)
    private String path;

    /**
     * The field <code>note</code> contains optional notes on this address.
     */
    @Column(length = 4096)
    private String notes;

    /**
     * The field <code>status</code> contains the status of the address.
     */
    @Column
    @Enumerated(EnumType.STRING)
    private Status status;

    /**
     * The field <code>probes</code> contains the list of probes.
     */
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "address_id")
    @Builder.Default
    @JsonIgnore
    private List<Probe> probes = new ArrayList<>();

    /**
     * The field <code>summary</code> contains the embedded statistics cache.
     */
    @Embedded()
    @AttributeOverrides({ //
            @AttributeOverride(name = "count", //
                    column = @Column(name = "summary_count")),
            @AttributeOverride(name = "active", //
                    column = @Column(name = "summary_active")) })
    private ProbeSummary summary;

    /**
     * The method <code>add</code> provides means to add a probe to the address.
     *
     * @param p the probe to be added
     */
    public void add(Probe p) {

        p.setAddress(this);
        probes.add(p);
    }
}
