/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */

package org.ctan.disposer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The class <code>Account</code> contains an user account.
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Account {

    @Id
    @GeneratedValue
    private Long id;
    
    @Column(length = 128)
    private String account;
    
    @Column(length = 255)
    private String password;
    
    @Column(length = 255)
    private String name;
    
    @Column(length = 255)
    private String email;
    
    @Column(length = 32)
    private String roles;
    
    @Column
    @Builder.Default
    private boolean locked = false;
}
