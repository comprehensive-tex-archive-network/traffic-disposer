/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */

package org.ctan.disposer.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The class <code>Mirror</code> represents a mirror server.
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Server {

    /**
     * The class <code>Status</code> represents the status of a server.
     */
    public enum Status {
        MASTER, //
        ACTIVE, //
        PARTIAL, //
        INACTIVE, //
        WITHDRAWN
    }

    /**
     * The field <code>id</code> contains the numeric id.
     */
    @Id
    @GeneratedValue
    private Long id;

    /**
     * The field <code>name</code> contains the DNS name of the server.
     */
    @Column(length = 255)
    private String name;

    /**
     * The field <code>region</code> contains the optional region.
     */
    @Column(length = 128)
    private String region;

    /**
     * The field <code>town</code> contains the town in which the server is
     * located.
     */
    @Column(length = 128)
    private String town;

    /**
     * The field <code>dateAdded</code> contains the optional time when the
     * mirror has been added.
     */
    @Column
    private LocalDateTime dateAdded;

    /**
     * The field <code>dateRemoved</code> contains the optional time stamp when
     * the server has been withdrawn. If this value is set then the status
     * should be WITHDRAWN.
     */
    @Column
    private LocalDateTime dateRemoved;

    /**
     * The field <code>notes</code> contains free text notes.
     */
    @Column(length = 4096)
    private String notes;

    /**
     * The field <code>country</code> contains the country code of the server.
     */
    @Column
    @Enumerated(EnumType.STRING)
    private Country country;

    /**
     * The field <code>status</code> contains the status of the server.
     */
    @Column
    @Enumerated(EnumType.STRING)
    private Status status;

    /**
     * The field <code>addresses</code> contains the addresses under which the
     * server can be reached.
     */
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "server_id")
    private List<Address> addresses;

    /**
     * The field <code>contacts</code> contains the optional list of contacts.
     */
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "server_id")
    private List<Contact> contacts;

}
