/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */

package org.ctan.disposer.model;

import java.io.IOException;
import java.time.LocalDateTime;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPSClient;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.ctan.disposer.model.Probe.Status;

import lombok.extern.slf4j.Slf4j;

/**
 * The class <code>Protocol</code> represents one of the supported protocols.
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
@Slf4j
public enum Protocol {
    /**
     * The value <code>HTTP</code> represents the HTTP protocol.
     */
    HTTP {
        /**
         * @see org.ctan.disposer.model.Protocol#getFallbacks()
         */
        @Override
        public Protocol[] getFallbacks() {

            return new Protocol[] { HTTPS };
        }

        /**
         * @see org.ctan.disposer.model.Protocol#probe(java.lang.String,
         *          java.lang.String)
         */
        @Override
        public Probe probe(String hostname, String path) {

            if (path.length() > 0 && !path.startsWith("/")) {
                path = "/" + path;
            }

            String url = "http://" + hostname + path;
            try {
                HttpClient client = HttpClientBuilder.create().build();
                HttpResponse response = client.execute(new HttpGet(url));
                switch (response.getStatusLine().getStatusCode()) {
                case HttpStatus.SC_OK:
                    return Probe.builder() //
                            .status(Status.OK) //
                            .time(LocalDateTime.now()) //
                            .build();
                case HttpStatus.SC_INTERNAL_SERVER_ERROR:
                default:
                    return Probe.builder() //
                            .status(Status.ERROR) //
                            .time(LocalDateTime.now()) //
                            .build();
                }
            } catch (IOException e) {
                log.info(url + ": " + e.getMessage());
                return Probe.builder() //
                        .status(Status.ERROR) //
                        .time(LocalDateTime.now()) //
                        .build();
            }
        }
    },
    /**
     * The value <code>HTTPS</code> represents the HTTPS protocol.
     */
    HTTPS {
        /**
         * @see org.ctan.disposer.model.Protocol#getFallbacks()
         */
        @Override
        public Protocol[] getFallbacks() {

            return new Protocol[] { HTTP };
        }

        /**
         * @see org.ctan.disposer.model.Protocol#probe(java.lang.String,
         *          java.lang.String)
         */
        @Override
        public Probe probe(String hostname, String path) {

            if (path.length() > 0 && !path.startsWith("/")) {
                path = "/" + path;
            }

            String url = "https://" + hostname + path;
            try {
                HttpClient client = HttpClientBuilder.create().build();
                HttpResponse response = client.execute(new HttpGet(url));
                Header[] lastModified = response.getHeaders("last-modified");
                if (lastModified.length > 0) {
                    lastModified[0].getValue();
                    // TODO
                }
                switch (response.getStatusLine().getStatusCode()) {
                case HttpStatus.SC_OK:
                    return Probe.builder() //
                            .status(Status.OK) //
                            .time(LocalDateTime.now()) //
                            .build();
                case HttpStatus.SC_INTERNAL_SERVER_ERROR:
                default:
                    return Probe.builder() //
                            .status(Status.ERROR) //
                            .time(LocalDateTime.now()) //
                            .build();
                }
            } catch (IOException e) {
                log.info(url + ": " + e.getMessage());
                return Probe.builder() //
                        .status(Status.ERROR) //
                        .time(LocalDateTime.now()) //
                        .build();
            }
        }
    },
    /**
     * The value <code>FTP</code> represents the FTP protocol.
     */
    FTP {
        /**
         * @see org.ctan.disposer.model.Protocol#getFallbacks()
         */
        @Override
        public Protocol[] getFallbacks() {

            return new Protocol[] {};
        }

        /**
         * @see org.ctan.disposer.model.Protocol#probe(java.lang.String,
         *          java.lang.String)
         */
        @Override
        public Probe probe(String hostname, String path) {

            FTPClient ftp = new FTPClient();
            try {

                ftp.connect(hostname);
                ftp.enterLocalPassiveMode();
                ftp.setFileType(
                        org.apache.commons.net.ftp.FTP.BINARY_FILE_TYPE);

                ftp.listDirectories(path);
                return Probe.builder() //
                        .status(Status.OK) //
                        .time(LocalDateTime.now()) //
                        .build();
            } catch (IOException ex) {
                return Probe.builder() //
                        .status(Status.ERROR) //
                        .time(LocalDateTime.now()) //
                        .build();
            } finally {
                try {
                    if (ftp.isConnected()) {
                        ftp.disconnect();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    },
    /**
     * The value <code>SFTP</code> represents the SFTP protocol.
     */
    SFTP {
        /**
         * @see org.ctan.disposer.model.Protocol#getFallbacks()
         */
        @Override
        public Protocol[] getFallbacks() {

            return new Protocol[] {};
        }

        /**
         * @see org.ctan.disposer.model.Protocol#probe(java.lang.String,
         *          java.lang.String)
         */
        @Override
        public Probe probe(String hostname, String path) {

            FTPSClient sftp = new FTPSClient();
            try {

                sftp.connect(hostname);
                sftp.enterLocalPassiveMode();
                sftp.setFileType(
                        org.apache.commons.net.ftp.FTP.BINARY_FILE_TYPE);

                sftp.listDirectories(path);
                return Probe.builder() //
                        .status(Status.OK) //
                        .time(LocalDateTime.now()) //
                        .build();
            } catch (IOException ex) {
                return Probe.builder() //
                        .status(Status.ERROR) //
                        .time(LocalDateTime.now()) //
                        .build();
            } finally {
                try {
                    if (sftp.isConnected()) {
                        sftp.disconnect();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    },
    /**
     * The value <code>RSYNC</code> represents the RSYNC protocol.
     */
    RSYNC {
        /**
         * @see org.ctan.disposer.model.Protocol#getFallbacks()
         */
        @Override
        public Protocol[] getFallbacks() {

            return new Protocol[] {};
        }

        /**
         * @see org.ctan.disposer.model.Protocol#probe(java.lang.String,
         *          java.lang.String)
         */
        @Override
        public Probe probe(String hostname, String path) {

            log.debug("rsync://" + hostname);
            // TODO
            return Probe.builder() //
                    .status(Status.NOT_REACHABLE) //
                    .time(LocalDateTime.now()) //
                    .build();
        }
    };

    /**
     * The method <code>getFallbacks</code> provides means to get fallback
     * protocols in case the requested protocol does not work.
     *
     * @return a list of fallback protocols
     */
    public abstract Protocol[] getFallbacks();

    /**
     * The method <code>probe</code> provides means to send one request to the
     * server.
     *
     * @param hostname the host name
     * @param path     the path
     * @return the probe result
     */
    public abstract Probe probe(String hostname, String path);

    /**
     * Return the printable representation in all lower case.
     *
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {

        return name().toLowerCase();
    }
}
