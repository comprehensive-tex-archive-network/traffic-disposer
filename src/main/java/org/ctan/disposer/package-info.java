/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */

/**
 * This package contains the CTAN traffic disposer. The main task is to monitor
 * the mirror servers and dispatch requests to a server nearby. In addition the
 * data collected in the database can be edited by an authorised user.
 */
package org.ctan.disposer;
