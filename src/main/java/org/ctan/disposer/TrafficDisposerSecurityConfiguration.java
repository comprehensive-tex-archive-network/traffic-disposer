/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */

package org.ctan.disposer;

import org.ctan.disposer.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

/**
 * The class <code>TrafficDisposerSecurityConfiguration</code> contains...TODO
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(jsr250Enabled = true)
public class TrafficDisposerSecurityConfiguration
        extends WebSecurityConfigurerAdapter {

    /**
     * The field <code>userDetailsService</code> contains the injected
     * UserDetailsService.
     */
    private AuthService authService;

    /**
     * This is the constructor for the class
     * <code>TrafficDisposerSecurityConfiguration</code>.
     *
     * @param authService the authentication service
     */
    @Autowired
    public TrafficDisposerSecurityConfiguration(AuthService authService) {

        this.authService = authService;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.security.config.annotation.web.configuration.
     * WebSecurityConfigurerAdapter#configure(org.springframework.security.
     * config.annotation.authentication.builders.AuthenticationManagerBuilder)
     */
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.userDetailsService(authService)
                .passwordEncoder(AuthService.B_CRYPT_PASSWORD_ENCODER);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.security.config.annotation.web.configuration.
     * WebSecurityConfigurerAdapter#configure(org.springframework.security.
     * config.annotation.web.builders.HttpSecurity)
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http //
                .cors() //
                .and() //
                .csrf().disable() //
                .authorizeRequests().antMatchers("/", //
                        "/favicon.ico", //
                        "/**/*.png", //
                        "/**/*.gif", //
                        "/**/*.svg", //
                        "/**/*.jpg", //
                        "/**/*.html", //
                        "/**/*.css", //
                        "/**/*.js", //
                        "/fonts/**/*")
                .permitAll() //
                .antMatchers(HttpMethod.POST, "/api/login") //
                .permitAll() //
//                .anyRequest().authenticated() //
                .and() //
                .addFilter(new JwtAuthenticationFilter(authenticationManager(),
                        authService))
                // this disables session creation on Spring Security
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    /**
     * The method <code>corsConfigurationSource</code> provides the CORS
     * configuration source.
     *
     * @return the source
     */
    @Bean
    CorsConfigurationSource corsConfigurationSource() {

        final UrlBasedCorsConfigurationSource source = //
                new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**",
                new CorsConfiguration().applyPermitDefaultValues());
        return source;
    }

}
