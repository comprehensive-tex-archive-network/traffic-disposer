/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */

package org.ctan.disposer.service;

import java.util.Collections;
import java.util.Set;

import org.ctan.disposer.model.Account;
import org.ctan.disposer.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * The class <code>AuthService</code> contains methods for A&amp;A.
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
@Service
@Slf4j
public class AuthService implements UserDetailsService {

    /**
     * The class <code>AuthUserDetails</code> contains...TODO
     */
    @Getter
    @AllArgsConstructor
    @Builder
    public static class AuthUserDetails implements UserDetails {

        /**
         * The field <code>serialVersionUID</code> contains serialization
         * version number.
         */
        private static final long serialVersionUID = 1L;

        private Set<GrantedAuthority> authorities;
        @Builder.Default
        private boolean enabled = true;
        private String name;
        private String password;
        private String username;

        @Override
        public boolean isAccountNonExpired() {

            return true;
        }

        @Override
        public boolean isAccountNonLocked() {

            return true;
        }

        @Override
        public boolean isCredentialsNonExpired() {

            return true;
        }
    }

    /**
     * The field <code>B_CRYPT_PASSWORD_ENCODER</code> contains the encoder.
     */
    public static final BCryptPasswordEncoder B_CRYPT_PASSWORD_ENCODER = //
            new BCryptPasswordEncoder(11);

    private AccountRepository accounts;

    /**
     * The field <code>algorithm</code> contains the algorithm to be used to
     * sign the JWT.
     */
    private final Algorithm algorithm;

    private String issuer;

    /**
     * This is the constructor for the class <code>AuthService</code>.
     *
     * @param accounts the accounts repo
     */
    @Autowired
    public AuthService(AccountRepository accounts, Environment env) {

        this.accounts = accounts;
        this.algorithm = Algorithm
                .HMAC256(env.getProperty("traffic-disposer.jwt.secret"));
        this.issuer = env.getProperty("traffic-disposer.jwt.issuer");
    }

    /**
     * The method <code>createJwt</code> provides means to create a JWT for a
     * user.
     *
     * @param account the user name
     * @return the JWT
     */
    public String createJwt(String account) {

        return JWT.create() //
                .withSubject(account) //
                .withIssuer(issuer) //
                // .withExpiresAt(new Date(
                // System.currentTimeMillis() + JwtUtils.EXPIRATION_TIME))
                .sign(algorithm);
    }

    /**
     * The method <code>encodePassword</code> provides means to encrypt a
     * password.
     *
     * @param account the account
     * @param passwd  the unencrypted password
     * @return the encrypted password
     */
    public String encodePassword(String account, String passwd) {

        return B_CRYPT_PASSWORD_ENCODER
                .encode(preparePassword(account, passwd));
    }

    /**
     *
     */
    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {

        AuthUserDetails acc = userDetails(username);
        if (acc == null) {
            throw new UsernameNotFoundException(username);
        }

        return acc;
    }

    /**
     * The method <code>preparePassword</code> provides means to prepare the
     * password for encryption.
     *
     * @param account the account
     * @param passwd  the unencrypted password
     * @return the password for encryption
     */
    public String preparePassword(String account, String passwd) {

        return account + "**" + passwd;
    }

    /**
     * The method <code>userDetails</code> provides means to find details for a
     * user name.
     *
     * @param username the user name
     * @return details or <code>null</code> if the user is not found
     */
    public AuthUserDetails userDetails(String username) {

        Account account = accounts.findByAccount(username);

        if (account == null) {
            return null;
        }

        return AuthUserDetails.builder() //
                .username(username) //
                .password(account.getPassword()) //
                .name(account.getName()) //
                .enabled(!account.isLocked()) //
                .authorities(Collections.emptySet()) // fixme
                .build();
    }

    /**
     * The method <code>verifyPassword</code> provides means to check a password
     * against an encrypted version.
     *
     * @param account the account
     * @param passwd  the unencrypted password
     * @return the details iff the passwords match
     */
    public AuthUserDetails verifyPassword(String account, String passwd) {

        AuthUserDetails acc = userDetails(account);
        if (acc == null || !(B_CRYPT_PASSWORD_ENCODER.matches(
                preparePassword(account, passwd), acc.getPassword()))) {
            return null;
        }
        return acc;
    }
}
