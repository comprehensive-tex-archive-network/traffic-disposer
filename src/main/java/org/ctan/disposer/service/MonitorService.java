/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */

package org.ctan.disposer.service;

import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.transaction.Transactional;

import org.ctan.disposer.model.Address;
import org.ctan.disposer.model.Server;
import org.ctan.disposer.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 * The class <code>MonitorService</code> is a service to monitor servers.
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
@Service
@Slf4j
public class MonitorService {

    /**
     * The field <code>TRUST_ALL_CERTS</code> arranges things to avoid problems
     * with unknown certificates.
     */
    private static final TrustManager[] TRUST_ALL_CERTS = new TrustManager[] {
            new X509TrustManager() {

                @Override
                public void checkClientTrusted(X509Certificate[] arg0,
                        String arg1) throws CertificateException {

                }

                @Override
                public void checkServerTrusted(X509Certificate[] arg0,
                        String arg1) throws CertificateException {

                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {

                    return null;
                }
            } };

    /**
     * The field <code>addresses</code> contains the repository for server
     * addresses.
     */
    private AddressRepository addresses;

    /**
     * This is the constructor for the class <code>MirrorMonitorService</code>.
     *
     * @param addresses the addresses
     */
    @Autowired
    public MonitorService(AddressRepository addresses) {

        try {
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, TRUST_ALL_CERTS, new SecureRandom());
            HttpsURLConnection
                    .setDefaultSSLSocketFactory(sslContext.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.addresses = addresses;
    }

    /**
     * The method <code>probe</code> provides means to probe all server
     * addresses and record the results.
     */
    @Transactional
    public void probe() {

        addresses.findAll().forEach(ma -> {
            probe(ma);
        });
    }

    /**
     * The method <code>probe</code> provides means to probe a mirror with the
     * given id.
     *
     * @param id the id
     */
    @Transactional
    public void probe(Long id) {

        addresses.findAllByServerId(id).forEach(ma -> {
            probe(ma);
        });
    }

    /**
     * The method <code>probe</code> provides means to probe a single mirror
     * address and record the result.
     *
     * @param address the mirror address to probe
     */
    private void probe(Address address) {

        if (address.getServer().getStatus() == Server.Status.WITHDRAWN
                || address.getStatus() == Address.Status.WITHDRAWN) {
            return;
        }

        log.info(address.getServer().getName());
        address.add(address.getProtocol().probe(address.getServer().getName(),
                address.getPath()));
        addresses.save(address);
    }

}
