/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */

/**
 * This package contains services.
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
package org.ctan.disposer.service;
