/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */

package org.ctan.disposer.service;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.ctan.disposer.model.Address;
import org.ctan.disposer.model.Country;
import org.ctan.disposer.model.Protocol;
import org.ctan.disposer.model.Segment;
import org.ctan.disposer.model.Server;
import org.ctan.disposer.repository.AddressRepository;
import org.springframework.stereotype.Service;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.AddressNotFoundException;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.model.CountryResponse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * The class <code>GeoService</code> contains service methods for Geo location.
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
@Service
@Slf4j
public class GeoService {

    /**
     * The class <code>LocationData</code> is a transport object for location
     * data.
     */
    @Data
    @AllArgsConstructor
    @Builder
    public static class LocationData {

        private String ipAddress;
        private String continent;
        private String country;
        private String postal;
        private String city;
        private boolean anonymous;
        private List<Address> candidatesInCountry;
        private List<Address> candidatesInSegment;
        private List<Address> candidatesInWorld;
    }

    private static final File CITY_MMDB = new File(
            "/serv/www/www.ctan.org/GeoLite2/GeoLite2-City.mmdb");

    /**
     * The field <code>reader</code> contains the connection to the database.
     */
    private DatabaseReader reader;

    /**
     * The field <code>addresses</code> contains the repository for addresses.
     */
    private AddressRepository addresses;

    /**
     * This is the constructor for the class <code>GeoService</code>.
     *
     * @param addresses the address repository
     * @throws IOException in case of an I/O error
     */
    public GeoService(AddressRepository addresses) throws IOException {

        this.addresses = addresses;
        reader = new DatabaseReader.Builder(CITY_MMDB).build();
    }

    /**
     * The method <code>location</code> provides means to retrieve the location
     * of the current user.
     *
     * @param scheme the scheme
     * @param addr   the remote address
     * @return
     */
    public LocationData location(String scheme, String addr) {

        addr = "130.83.26.1"; // fixme
        CityResponse city;
        try {
            InetAddress ipAddress = InetAddress.getByName(addr);
            city = reader.city(ipAddress);
        } catch (UnknownHostException | AddressNotFoundException e) {
            return LocationData.builder() //
                    .anonymous(true) //
                    .build();
        } catch (IOException | GeoIp2Exception e) {
            // TODO Auto-generated catch block
            log.error("", e);
            return null;
        }

        Country country;
        try {
            country = Country.valueOf(city.getCountry().getIsoCode());
        } catch (Exception e) {
            country = null;
        }
        Segment segment = country == null ? null : country.getSegment();

        return LocationData.builder() //
                .anonymous(city.getTraits().isAnonymous()) //
                .ipAddress(city.getTraits().getIpAddress()) //
                .country(city.getCountry().getIsoCode()) //
                .continent(city.getContinent().getCode()) //
                .city(city.getCity().getName()) //
                .postal(city.getPostal().getCode()) //
                .candidatesInCountry(findAllInCountry(Protocol.HTTPS, country))
                .candidatesInSegment(findAllInSegment(Protocol.HTTPS, segment))
                .build();
    }

    /**
     * The method <code>redirectUrl</code> provides means to determine the
     * redirect URL for the current user.
     *
     * @param scheme the scheme (like "https")
     * @param addr   the address (IP or symbolic) of the current user
     * @return
     */
    public String redirectUrl(String scheme, String addr) {

        CountryResponse cr;
        try {
            InetAddress byName;
            byName = InetAddress.getByName(addr);
            cr = reader.country(byName);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            log.error("", e);
            return null;
        } catch (GeoIp2Exception e) {
            // TODO Auto-generated catch block
            log.error("", e);
            return null;
        }

        Protocol protocol = Protocol.valueOf(scheme.toUpperCase());
        Country country = Country.valueOf(cr.getCountry().getIsoCode());
        String a = findInCountry(protocol, country);
        if (a != null) {
            return a;
        }
        a = findInSegment(protocol, country.getSegment());
        if (a != null) {
            return a;
        }
        a = findInWorld(protocol);
        if (a != null) {
            return a;
        }

        return "https://ctan.org/texarchive"; // fixme make configurable
    }

    /**
     * The method <code>find</code> provides means to randomly select one
     * redirect URL from a list of candidates.
     *
     * @param protocol the protocol
     * @param cands    the list if candidates
     * @return a redirect URL or <code>null</code> if the list is empty
     */
    private String find(Protocol protocol, List<Address> cands) {

        if (cands.isEmpty()) {
            return null;
        }

        int n = (int) Math.floor(Math.random() * cands.size());
        Address ma = cands.get(n);
        return protocol.toString() + ":" + ma.getServer().getName()
                + ma.getPath();
    }

    /**
     * The method <code>findAllInCountry</code> provides means to TODO gene
     *
     * @param protocol the protocol
     * @param country  the country
     * @return
     */
    private List<Address> findAllInCountry(Protocol protocol, Country country) {

        if (country == null) {
            return Collections.emptyList();
        }
        return addresses.findAllByProtocolAndStatusAndCountry(protocol,
                Address.Status.ACTIVE, Server.Status.ACTIVE, country);
    }

    /**
     * The method <code>findAllInSegment</code> provides means to TODO gene
     *
     * @param protocol the protocol
     * @param segment  the segment
     * @return
     */
    private List<Address> findAllInSegment(Protocol protocol, Segment segment) {

        if (segment == null) {
            return Collections.emptyList();
        }
        return addresses
                .findAllByProtocolAndStatus(protocol, Address.Status.ACTIVE,
                        Server.Status.ACTIVE)
                .stream()
                .filter(m -> m.getServer().getCountry().getSegment() == segment)
                .collect(Collectors.toList());
    }

    /**
     * The method <code>findInCountry</code> provides means to find a redirect
     * candidate in the same country.
     *
     * @param protocol the protocol
     * @param country  the country
     * @return the URL or <code>null</code>
     */
    private String findInCountry(Protocol protocol, Country country) {

        return find(protocol, findAllInCountry(protocol, country));
    }

    /**
     * The method <code>findInSegment</code> provides means to find a redirect
     * candidate in the same segment.
     *
     * @param protocol the protocol
     * @param segment  the segment
     * @return the URL or <code>null</code>
     */
    private String findInSegment(Protocol protocol, Segment segment) {

        return find(protocol, findAllInSegment(protocol, segment));
    }

    /**
     * The method <code>findInWorld</code> provides means to find a redirect
     * candidate anywhere.
     *
     * @param protocol the protocol
     * @return the URL or <code>null</code>
     */
    private String findInWorld(Protocol protocol) {

        return find(protocol, addresses.findAllByProtocolAndStatus(protocol,
                Address.Status.ACTIVE, Server.Status.ACTIVE));
    }

    /**
     * The method <code>selectSome</code> provides means to TODO gene
     *
     * @param candidates
     * @param max
     * @return
     */
    private List<Address> selectSome(List<Address> candidates, int max) {

        Collections.shuffle(candidates);
        return candidates.subList(0, Math.min(max, candidates.size()));
    }
}
