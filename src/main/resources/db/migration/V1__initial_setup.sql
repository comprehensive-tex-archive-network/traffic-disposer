CREATE SEQUENCE public.hibernate_sequence
    INCREMENT 1
    START 1000
    MINVALUE 1000
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE public.server (
        id int8 NOT NULL,
        country varchar(255) NULL,
        date_added timestamp NULL,
        date_removed timestamp NULL,
        "name" varchar(255) NULL,
        notes varchar(4096) NULL,
        region varchar(128) NULL,
        status varchar(255) NULL,
        town varchar(128) NULL,
        CONSTRAINT mirror_pkey PRIMARY KEY (id)
);

CREATE TABLE public.address (
        id int8 NOT NULL,
        notes varchar(4096) NULL,
        "path" varchar(1024) NULL,
        protocol varchar(255) NULL,
        status varchar(16) NULL,
        server_id int8 NULL,
        summary_count integer NOT NULL,
        summary_active integer NOT NULL,
        CONSTRAINT address_pkey PRIMARY KEY (id)
);

ALTER TABLE public.address
        ADD CONSTRAINT fk_server_id
                FOREIGN KEY (server_id) REFERENCES server(id);

CREATE TABLE public.contact (
        id int8 NOT NULL,
        date_added timestamp NULL,
        date_removed timestamp NULL,
        email varchar(255) NULL,
        "name" varchar(255) NULL,
        notes varchar(4096) NULL,
        status varchar(16) NULL,
        server_id int8 NULL,
        CONSTRAINT contact_pkey PRIMARY KEY (id)
);

ALTER TABLE public.contact
        ADD CONSTRAINT fk_contact_mirror_id
                FOREIGN KEY (server_id) REFERENCES server(id);

CREATE TABLE public.probe (
        "time" timestamp NOT NULL,
        status varchar(16) NOT NULL,
        address_id int8 NOT NULL,
        CONSTRAINT probe_pkey PRIMARY KEY ("time")
);

ALTER TABLE public.probe
        ADD CONSTRAINT fk_address_id
                FOREIGN KEY (address_id) REFERENCES address(id);

CREATE TABLE public.account (
        id int8 NOT NULL,
        account varchar(128) NOT NULL,
        password varchar(255),
        name varchar(255),
        email varchar(255),
        roles varchar(32),
        locked boolean,
        CONSTRAINT account_pkey PRIMARY KEY (id)
);

INSERT INTO public.account (id, account, password, name, email, roles, locked)
VALUES (1,
        'admin',
        '$2a$11$SguNB1jNoF3nSYm6t0HDR.K3y9hX6zrcgVOAJoiP9xlp8tf1sCtwy',
        'A.D. Min',
        'admin@localhost',
        'A',
        false);
