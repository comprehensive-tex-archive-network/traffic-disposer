/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */
package org.ctan.disposer;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * The class <code>TrafficDisposerApplicationTests</code> containsa test suite.
 */
@SpringBootTest
class TrafficDisposerApplicationTests {

    @Test
    void contextLoads() {

    }
}
