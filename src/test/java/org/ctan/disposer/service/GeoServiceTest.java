/*
 * Copyright © 2020 The CTAN Team and individual authors
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */

package org.ctan.disposer.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.ctan.disposer.model.Address;
import org.ctan.disposer.model.Country;
import org.ctan.disposer.model.Protocol;
import org.ctan.disposer.model.Server;
import org.ctan.disposer.repository.AddressRepository;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * The class <code>GeoServiceTest</code> contains a test suite for the geo
 * service.
 *
 * @author <a href="gene@ctan.org">Gerd Neugebauer</a>
 */
class GeoServiceTest {

    class AddressRepositoryMock implements AddressRepository {

        private List<Address> all;

        /**
         * This is the constructor for the class
         * <code>AddressRepositoryMock</code>.
         */
        AddressRepositoryMock(Address... a) {

            all = Arrays.asList(a);
        }

        @Override
        public long count() {

            return all.size();
        }

        @Override
        public <S extends Address> long count(Example<S> example) {

            throw new RuntimeException("unsupported");
        }

        @Override
        public void delete(Address entity) {

            throw new RuntimeException("unsupported");
        }

        @Override
        public void deleteAll() {

            throw new RuntimeException("unsupported");
        }

        @Override
        public void deleteAll(Iterable<? extends Address> entities) {

            throw new RuntimeException("unsupported");
        }

        @Override
        public void deleteAllInBatch() {

            throw new RuntimeException("unsupported");
        }

        @Override
        public void deleteById(Long id) {

            throw new RuntimeException("unsupported");
        }

        @Override
        public void deleteInBatch(Iterable<Address> entities) {

            throw new RuntimeException("unsupported");
        }

        @Override
        public <S extends Address> boolean exists(Example<S> example) {

            throw new RuntimeException("unsupported");
        }

        @Override
        public boolean existsById(Long id) {

            throw new RuntimeException("unsupported");
        }

        @Override
        public List<Address> findAll() {

            return all;
        }

        @Override
        public <S extends Address> List<S> findAll(Example<S> example) {

            throw new RuntimeException("unsupported");
        }

        @Override
        public <S extends Address> Page<S> findAll(Example<S> example,
                Pageable pageable) {

            throw new RuntimeException("unsupported");
        }

        @Override
        public <S extends Address> List<S> findAll(Example<S> example,
                Sort sort) {

            throw new RuntimeException("unsupported");
        }

        @Override
        public Page<Address> findAll(Pageable pageable) {

            throw new RuntimeException("unsupported");
        }

        @Override
        public List<Address> findAll(Sort sort) {

            throw new RuntimeException("unsupported");
        }

        @Override
        public List<Address> findAllById(Iterable<Long> ids) {

            throw new RuntimeException("unsupported");
        }

        @Override
        public List<Address> findAllByProtocolAndStatus(Protocol protocol,
                Address.Status status, Server.Status serverStatus) {

            return all.stream()
                    .filter(ma -> ma.getProtocol() == protocol
                            && ma.getStatus() == status
                            && ma.getServer().getStatus() == serverStatus)
                    .collect(Collectors.toList());
        }

        @Override
        public List<Address> findAllByProtocolAndStatusAndCountry(
                Protocol protocol, Address.Status status,
                Server.Status serverStatus, Country country) {

            return all.stream()
                    .filter(ma -> ma.getProtocol() == protocol
                            && ma.getStatus() == status
                            && ma.getServer().getStatus() == serverStatus
                            && ma.getServer().getCountry() == country)
                    .collect(Collectors.toList());
        }

        @Override
        public List<Address> findAllByServerId(Long id) {

            return all.stream().filter(ma -> ma.getServer().getId() == id)
                    .collect(Collectors.toList());
        }

        @Override
        public Optional<Address> findById(Long id) {

            throw new RuntimeException("unsupported");
        }

        @Override
        public <S extends Address> Optional<S> findOne(Example<S> example) {

            throw new RuntimeException("unsupported");
        }

        @Override
        public void flush() {

        }

        @Override
        public Address getOne(Long id) {

            throw new RuntimeException("unsupported");
        }

        @Override
        public <S extends Address> S save(S entity) {

            throw new RuntimeException("unsupported");
        }

        @Override
        public <S extends Address> List<S> saveAll(Iterable<S> entities) {

            throw new RuntimeException("unsupported");
        }

        @Override
        public <S extends Address> S saveAndFlush(S entity) {

            throw new RuntimeException("unsupported");
        }
    }

    /**
     * Test that a redirect URL can be found.
     *
     * @throws IOException in case of an I/O error
     */
    @Test
    void test() throws IOException {

        AddressRepository addresses = new AddressRepositoryMock();
        GeoService service = new GeoService(addresses);
        String url = service.redirectUrl("http", "5.35.249.60");
        assertNotNull(url);
        assertEquals("https://", url.substring(0, 8));
    }

}
